package com.qdu;

import com.qdu.util.ErrorUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;

//如果需要自己定制spring配置
//可以自己创建一个spring配置类
//但是如果只是开启Mapper包的扫描，也可直接使用启动类这个配置类
//@MapperScan注解用于指定Mapper接口所在的包
//这样mybatis-spring会自动为包下的每个Mapper接口生成映射器实例
@EnableSwagger2
@MapperScan(basePackages= {"com.qdu.mapper"})
@SpringBootApplication
public class SsmApplication {
	
    public static void main(String[] args) {
        SpringApplication.run(SsmApplication.class, args);
    }
//    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        PaginationInterceptor page = new PaginationInterceptor();
//        page.setDialectType("mysql");
//        return page;
//    }
}
@Slf4j
@Controller
@RequestMapping("/")
@Configuration
class IndexController{


    /**
     * 端口
     */
    @Value("${server.port}")
    private String port;

    /**
     * 启动成功
     */
    @Bean
    public ApplicationRunner applicationRunner() {
        return applicationArguments -> {
            try {

                //获取本机内网IP
                log.info("##管理员页面：" + "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port+"/back_login" );
                log.info("----------------------------------------------------------------");
                log.info("##HR页面：" + "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port+"/hrlogin" );
                log.info("----------------------------------------------------------------");
                log.info("启动成功：" + "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port+"/login" );
            } catch (UnknownHostException e) {
                //输出到日志文件中
                log.error(ErrorUtil.errorInfoToString(e));
            }
        };
    }

    /**
     * 端口
     */
    @GetMapping("view/system/logging_service.html")
    public ModelAndView logging() {
        return new ModelAndView("back-stage-management/view/system/logging_service","port",port);
    }
}

