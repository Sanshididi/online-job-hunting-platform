package com.qdu.aop.sys;

import com.qdu.annotation.AddLoginLog;
import com.qdu.codes.BaseConstants;
import com.qdu.codes.ErrorEnum;
import com.qdu.entity.sys.log.LogLogin;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.service.sys.log.LogLoginService;
import com.qdu.util.IpUtil;
import com.qdu.vo.ResultVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author:sanShi
 * @Description:{日志AOP}
 * @date :2020/12/14
 */
@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class LogAspect {
    private final HttpServletRequest request;
    private final LogLoginService logLoginService;

    @Pointcut("@annotation(com.qdu.annotation.AddLoginLog)")
    @Order(1)
    public void addElasticsearchLog() {
    }

    /**
     * 返回通知（正常返回）
     */
    @AfterReturning(value = "addElasticsearchLog()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        // 开始方法
        ResultVO result1 = (ResultVO) result;
        if (ErrorEnum.SUCCESS.getCode().equals((result1.getCode()))) {
            AddLoginLog loginLog = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(AddLoginLog.class);
            val sysAccount = (SysAccount) request.getSession().getAttribute(BaseConstants.USER_SESSION);
            LogLogin logLogin = new LogLogin();
            logLogin.setAccountId(sysAccount.getId())
                    .setCode(sysAccount.getCode())
                    .setName(sysAccount.getName())
                    .setIp(IpUtil.getIpAddr(request))
                    .setCategoryId(loginLog.logLoginCategory().getCategoryId())
                    .setCategoryName(loginLog.logLoginCategory().getCategoryName())
                    .setIsDeleted(sysAccount.getIsDeleted());
            val rst = logLoginService.save(logLogin);
            log.info("记录登录日志..结果:" + rst);
        }
    }
}
