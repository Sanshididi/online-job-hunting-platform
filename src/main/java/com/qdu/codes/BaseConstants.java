package com.qdu.codes;

public abstract class BaseConstants {
    /**
     * 已删除标识
     */
    public static final boolean DELETE_FLAG = true;
    /**
     * 未删除标识
     */
    public static final boolean NOT_DELETE_FLAG = false;
    /**
     * 登录用户的资源 session key
     */
    public static final String USER_RESOURCE_KEY = "userResourceKey";
    /**
     * 登录用户 session key
     */
    public static final String USER_SESSION = "userSession";
    /**
     * session 失效时间 8小时
     */
    public static final int SESSION_TIME_OUT = 28800;
}
