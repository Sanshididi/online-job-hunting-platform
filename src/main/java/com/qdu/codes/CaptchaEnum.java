package com.qdu.codes;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author:Sasnhi
 * @Description:{阿里云短信服务模板信息}
 * @date :2020/12/16
 */
@Getter
@AllArgsConstructor
public enum CaptchaEnum {
    /**
     * 验证码模版参数
     */
    TEMPLATE_REG(1L, "注册验证码", "Jerry帮帮求职", "SMS_206865041"),
    TEMPLATE_LOGIN(2L, "登录验证码", "Jerry帮帮求职", "SMS_206895028"),
    TEMPLATE_CHANGE(3L, "修改密码验证码", "Jerry帮帮求职", "SMS_206890032"),
    TEMPLATE_ALLOW(4L, "申请通过", "Jerry帮帮求职", "SMS_207952515"),
    TEMPLATE_REJECT(5L, "申请未通过", "Jerry帮帮求职", "SMS_207947542");


    private final Long id;
    private final String name;
    private final String signName;
    private final String templateCode;
}
