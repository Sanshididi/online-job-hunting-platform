package com.qdu.codes;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author:sanShi
 * @Description:{日志}
 * @date :2020/12/12
 */
@Getter
@AllArgsConstructor
public enum LogLoginCategoryEnum {

    /**
     * 登录类型
     */
    REGISTER_LOGIN(0L, "注册登录"),
    PASSWORD_LOGIN(1L, "密码登录"),
    CAPTCHA_LOGIN(2L, "验证码登录");

    private final Long categoryId;
    private final String categoryName;
}
