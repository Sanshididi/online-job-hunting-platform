package com.qdu.param.captcha;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/31
 */
@Data
public class CaptchaResponse implements Serializable {
    private String Message;
    private String RequestId;
    private String BizId;
    private String Code;
}
