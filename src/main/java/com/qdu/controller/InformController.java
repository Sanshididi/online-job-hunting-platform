package com.qdu.controller;

import com.qdu.entity.Inform;
import com.qdu.entity.Resumestate;
import com.qdu.mapper.PositionneedMapper;
import com.qdu.mapper.UsersMapper;
import com.qdu.service.FirmService;
import com.qdu.service.InformService;
import com.qdu.service.PositionneedService;
import com.qdu.util.SendMessages;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.Date;

/**
 * <p>
 *  个人中心相关控制器
 * </p>
 *
 * @author Zom
 * @since 2020-12-11
 */

@Controller
@RequestMapping("/inform")
@RequiredArgsConstructor
public class InformController {

    @Autowired
    private InformService informService;

    @Autowired
    private PositionneedMapper positionneedMapper;

    @Autowired
    private PositionneedService positionneedService;

    @Autowired
    private UsersMapper usersMapper;

    /**根布当前登录用户查询通知表，返回的列表显示在个人中心页面上*/
    @GetMapping("/listinform")
    public String findinformlist(String receiver, Model model) {
        System.out.println("--------"+receiver);
        model.addAttribute("informList", informService.getInformListByReceiver(receiver));
        model.addAttribute("rsList", informService.getRsListByReceiver(receiver));
        model.addAttribute("favoriteList", positionneedMapper.selectByReceiver(receiver));

        int uid = Integer.parseInt(receiver);
        model.addAttribute("usermess", usersMapper.selectByPrimaryKey(uid));
        return "personalcenter";
    }

    /**根布当前登录用户查询投递状态表，返回的列表显示在个人中心页面上*/
    @GetMapping("/listrs")
    public String findrslist(String receiver, Model model) {
        System.out.println("--------"+receiver);
        model.addAttribute("informList", informService.getInformListByReceiver(receiver));

        return null;
    }

    /**根据通知号删除一个学生，页面上发送ajax请求*/
    @PostMapping("/deleteinform")
    @ResponseBody
    public void delete(String informid) {
        System.out.println("----controller中收到的informid："+informid);
        informService.deleteInform(informid);
    }

    /**删除收藏的职位*/
    @PostMapping("/deleteFavorite")
    @ResponseBody
    public void deleteFavorite(String positionID) {
        System.out.println("----controller中收到的informid："+positionID);
        positionneedMapper.deleteFavorite(positionID);
    }

    /**根布当前登录用户查询收藏表，返回的列表显示在个人中心页面上*/
    @GetMapping("/favorite")
    public String addFavorite(String receiver, String positionID) {
        System.out.println("----添加收藏收到的receiver----"+receiver);
        System.out.println("----添加收藏收到的positionID----"+positionID);

        positionneedMapper.updateByPositionId(positionID,receiver);

        return "personalcenter";
    }

    /**根布当前登录用户查询状态表，返回的列表显示在个人中心页面上*/
    @GetMapping("/rState")
    public String addResumeState(String receiver, Integer positionID,String salary,String Pname,String Fname,Integer status, Model model,String tittle,String value) throws MessagingException {
        val job=positionneedService.getById(positionID);

        salary= job.getPsalary();
        Pname= job.getPname();
        Fname="什么公司";
        status=0;

        Resumestate r=new Resumestate();

        r.setSalary(salary);
        r.setPname(Pname);
        r.setFname(Fname);
        r.setStatus(status);
        r.setReceiver(receiver);
        r.setCreatetime(new Date());
        r.setPositionid(positionID);

        informService.addResumeState(r);

        addInform(receiver,positionID,Pname,status);

        tittle="帮帮求职";
        value="您发布的"+Pname+"收到了新的申请，请及时查看。-----帮帮求职-----";

        SendMessages.Send(tittle,value);
        return "personalcenter";
    }


    /**添加通知*/
    @GetMapping("/addinfo")
    public String addInform(String receiver, Integer positionID,String Pname,Integer status){
        val job=positionneedService.getById(positionID);
        Pname= job.getPname();

        Inform info=new Inform();
        info.setPositionId(positionID);
        info.setCreatetime(new Date());
        info.setTitle("求职小助手通知");
        info.setReceiver(receiver);
        info.setContent("您向 "+job.getPname()+" 投递的简历，状态已更新，请及时查看。");

        informService.addInform(info);

        return "personalcenter";
    }
}
