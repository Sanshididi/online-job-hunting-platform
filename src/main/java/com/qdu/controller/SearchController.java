package com.qdu.controller;



import com.qdu.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private SearchService searchService;


    @RequestMapping("/to_search")
    public String toResume() {
        return "search";
    }




    @GetMapping("/findPositionneed")
    public String findPositionneed(Model model) {
        model.addAttribute("PositionneedList", searchService.getPositionneedList());


        return "search";
    }



//    //根据编号查询单个简历信息，然后跳转到resumeplus.jsp页面显示信息，方便编辑
//    @GetMapping("/to_/{rId}")
//    public String to(@PathVariable String pId, Model model) {
//        model.addAttribute("Positionneed", searchService.getPositionneedById(pId));
//
//        return "";
//    }

    @GetMapping("/to_searchp")
    public String tosearchp(String pname, Model model) {
        model.addAttribute("Positionneed", searchService.getPositionneedBypname(pname));

        return "search2";
    }


}
