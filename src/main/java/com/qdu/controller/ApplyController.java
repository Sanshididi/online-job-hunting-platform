package com.qdu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qdu.entity.Apply;
import com.qdu.entity.Positionneed;
import com.qdu.service.ApplyService;
import com.qdu.service.InformService;
import com.qdu.service.PositionneedService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Controller
@RequestMapping("/apply")
public class ApplyController {

    @Autowired
    private ApplyService applyService;

    @Autowired
    private PositionneedService positionneedService;

    @Autowired
    private InformService informService;



    @PostMapping("/success")
    @ResponseBody
    public void successApply(int aid,String receiver, Integer positionID){
        val job=positionneedService.getById(positionID);
        receiver=job.getReceiver();
        positionID=job.getPositionID();
        informService.updateResumeStatus(positionID,receiver,"3");


        Apply apply=applyService.getById(aid);
        apply.setAisemploy(1);
        applyService.save(apply);

        Apply a=applyService.getById(aid);
        int pid=a.getPosititionID();

        Positionneed positionneed=positionneedService.getById(pid);
        int appliedid=positionneed.getPapplied();
        int appliedid2=appliedid+1;
        positionneed.setPapplied(appliedid2);
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("positionID",pid);
        positionneedService.update(positionneed,wrapper);

        applyService.removeById(aid);
    }

    @PostMapping("/fail")
    @ResponseBody
    public void failApply(int aid,String receiver, Integer positionID){
        val job=positionneedService.getById(positionID);
        receiver=job.getReceiver();
        positionID=job.getPositionID();
        informService.updateResumeStatus(positionID,receiver,"2");

        Apply apply=applyService.getById(aid);
        apply.setAisemploy(0);
        applyService.save(apply);
        applyService.removeById(aid);
    }


}

