package com.qdu.controller.BackstageViewController;

import com.qdu.codes.BaseConstants;
import com.qdu.entity.sys.sys.SysAccount;
import lombok.val;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 * @author:sanShi
 * @Description:{}
 * @date :2020/12/1
 */
public abstract class BaseController implements ServletContextAware {
    @Override
    public void setServletContext(ServletContext servletContext) {

    }

    /**
     * 获取当前登录用户
     *
     * @param session session
     * @return 用户
     */
    protected SysAccount getLoginUser(HttpSession session) {
        val userVo = session.getAttribute(BaseConstants.USER_SESSION);
        if (userVo == null) {
            throw new RuntimeException("没有登录信息");
        } else {
            return (SysAccount) userVo;
        }
    }

}
