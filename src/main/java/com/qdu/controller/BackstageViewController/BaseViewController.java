package com.qdu.controller.BackstageViewController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author:sanShi
 * @Description:{控制后台页面，接受后台页面切换指令，并且正确跳转}
 * @date :2020/11/30
 */
@Controller
@RequestMapping("/view")
public class BaseViewController {

//    console文件夹/对应页面：控制台

//    @GetMapping("/console/console1.html")
//    public String navigateConsole1() {
//        return "back-stage-management/view/console/console1";
//    }

    @GetMapping("/console/console2.html")
    public String navigateConsole2() {
        return "back-stage-management/view/console/console2";
    }

    //system文件夹/对应页面：系统管理
    @GetMapping("/system/user.html")
    public String navigateSystem1() {
        return "back-stage-management/view/system/user";
    }

    @GetMapping("/system/hr.html")
    public String navigateSystem2_1() {
        return "back-stage-management/view/system/hr";
    }
    @GetMapping("/system/role.html")
    public String navigateSystem2() { return "back-stage-management/view/system/role"; }

    @GetMapping("/system/power.html")
    public String navigateSystem3() {
        return "back-stage-management/view/system/power";
    }

    @GetMapping("/system/deptment.html")
    public String navigateSystem4() {
        return "back-stage-management/view/system/deptment";
    }

    @GetMapping("/system/logging.html")
    public String navigateSystem5() {
        return "back-stage-management/view/system/logging";
    }

    @GetMapping("/system/dict.html")
    public String navigateSystem6() {
        return "back-stage-management/view/system/dict";
    }

//    @GetMapping("/system/logging_service.html")
//    public ModelAndView logging() {
//        return new ModelAndView("back-stage-management/view/system/logging_service","port",8099);
//    }


}
