package com.qdu.controller.BackstageViewController.entry.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qdu.entity.sys.log.LogLogin;
import com.qdu.service.sys.log.LogLoginService;
import com.qdu.vo.PageBean;
import com.qdu.vo.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:Sasnhi
 * @Description:{登录日志}
 * @date :2020/12/14
 */
@RestController
@RequestMapping("/api/admin/setting/logLogin")
@Api(value = "登录日志")
@RequiredArgsConstructor
public class LogLoginApiController {
    private final LogLoginService logLoginService;

    @GetMapping("/list")
    @ApiOperation(value = "获取列表数据")
    public PageVO<LogLogin> list(PageBean pageBean) {
        val wrapper = new QueryWrapper<LogLogin>();
        LambdaQueryWrapper<LogLogin> queryWrapper = wrapper.lambda();
        queryWrapper.orderByDesc(LogLogin::getCreateTime);
        IPage<LogLogin> logLoginPage = logLoginService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(logLoginPage.getRecords(), logLoginPage.getTotal());
    }
}
