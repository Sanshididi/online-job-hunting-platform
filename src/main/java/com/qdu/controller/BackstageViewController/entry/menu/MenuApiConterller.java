package com.qdu.controller.BackstageViewController.entry.menu;

import com.qdu.entity.sys.role.Resource;
import com.qdu.service.sys.Role.ResourceService;
import com.qdu.vo.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/api/menu")
@Api(value = "接口管理")
@RequiredArgsConstructor
public class MenuApiConterller {
    private final ResourceService sysResourceService;

    @ApiOperation(value = "获取列表数据")
    @GetMapping("/list")
    public PageVO<Resource> list() {
        List<Resource> list = sysResourceService.list();
        return PageVO.pageVO(list, (long) list.size());
    }
}
