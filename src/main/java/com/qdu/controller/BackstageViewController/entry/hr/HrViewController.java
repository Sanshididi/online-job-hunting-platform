package com.qdu.controller.BackstageViewController.entry.hr;

import com.qdu.entity.sys.role.Role;
import com.qdu.entity.sys.role.RolesHr;
import com.qdu.service.FirmService;
import com.qdu.service.HrService;
import com.qdu.service.sys.Role.RoleService;
import com.qdu.service.sys.Role.RolesHrService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hr")
@RequiredArgsConstructor
public class HrViewController {

   private final FirmService firmService;
   private final RoleService roleService;
   private final RolesHrService rolesHrService;
   private final HrService hrService;
    /**
     * HR申请页面
     *
     * @return HR申请页面
     */
    @GetMapping("/registerPage")
    public String registerPage(Model model) {
        return "back-stage-management/HrRegister";
    }
    /**
     * 查看Hr公司信息
     *
     * @return 查看Hr公司信息
     */
    @GetMapping("/detail/{fid}/{hid}")
    @ApiOperation(value = "查看公司信息")
    public String edit(Model model,
                       @PathVariable Long fid,
                       @PathVariable Long hid) {
        val Hr=hrService.getById(hid);
        val Firm = firmService.getById(fid);
        model.addAttribute("Frim", Firm);
        model.addAttribute("hr", Hr);
        return "back-stage-management/view/system/operate/detail";
    }
    @GetMapping("/power/{hid}")
    @ApiOperation(value = "得到权限列表")
    public String power(Model model,
                        @PathVariable Long hid) {
        val hr = hrService.getById(hid);
        model.addAttribute("hr", hr);
        val roles = roleService.list();
        val rolesHr = rolesHrService.findByHrId(hid);
        for (Role role : roles) {
            for (RolesHr RolesHr : rolesHr) {
                if (RolesHr.getRoleId().equals(role.getId())) {
                    role.setIsChecked(true);
                }
            }
        }
        model.addAttribute("roles", roles);

        return "back-stage-management/view/system/operate/power";
    }
}
