package com.qdu.controller.BackstageViewController.entry.role;

import com.qdu.controller.BackstageViewController.BaseController;
import com.qdu.service.sys.Role.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/role/rolePermission")
@RequiredArgsConstructor
public class RoleViewController extends BaseController {
    private final RoleService sysRoleService;


    /**
     * 新增角色
     *
     * @return 新增角色
     */
    @GetMapping("/add")
    public String add() {
        return "back-stage-management/view/system/role/add";
    }

    /**
     * 编辑角色
     *
     * @return 编辑角色
     */
    @GetMapping("/edit")
    public String edit(Model model, Long id) {
        val sysRole = sysRoleService.getById(id);
        model.addAttribute("sysRole", sysRole);
        return "back-stage-management/view/system/role/edit";
    }
}
