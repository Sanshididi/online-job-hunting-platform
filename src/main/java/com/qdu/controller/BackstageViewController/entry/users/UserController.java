package com.qdu.controller.BackstageViewController.entry.users;

import com.qdu.service.sys.sys.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author:Sanshi
 * @Description:{控制用户list页面内的相关操作，如修改删除}
 * @date :2020/12/12
 */

@Controller
@RequestMapping("/operate")
@RequiredArgsConstructor
public class UserController {
    private final AccountService sysAccountService;

    /**
     * 用户新增页面
     *
     * @return 用户新增页面
     */
    @GetMapping("/add")
    public String add(Model model) {
        return "back-stage-management/view/system/operate/add";
    }

    /**
     * 用户编辑页面
     *
     * @param model 模型
     * @param id    id
     * @return 用户编辑页面
     */
    @GetMapping("/edit")
    public String edit(Model model, Long id) {
        val sysAccount = sysAccountService.getById(id);
        model.addAttribute("sysAccount", sysAccount);
        return "back-stage-management/view/system/operate/edit";
    }
}
