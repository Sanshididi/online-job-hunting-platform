package com.qdu.controller.BackstageViewController.entry.mainPage;

import com.qdu.entity.Firm;
import com.qdu.mapper.HrMapper;
import com.qdu.service.FirmService;
import com.qdu.service.HrService;
import com.qdu.service.PositionneedService;
import com.qdu.service.sys.log.LogLoginService;
import com.qdu.service.sys.msg.MsgRecordService;
import com.qdu.service.sys.sys.AccountService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@Controller
@Api(value = "主页数据")
@RequiredArgsConstructor
@RequestMapping("/view")
public class MainPageController {
    private final LogLoginService logLgoinService;
    private final PositionneedService positionneedService;
    private final FirmService firmService;
    private final AccountService accountService;
    @GetMapping("/console/console1.html")
    public String navigateConsole1(HttpSession session, Model model) {
        int pv=logLgoinService.count();
        int posit=positionneedService.count();
        int firm=firmService.count();
        int account=accountService.count();
        model.addAttribute("pv", pv);
        model.addAttribute("posit",posit);
        model.addAttribute("frim", firm);
        model.addAttribute("account", account);

        return "back-stage-management/view/console/console1";
    }

}
