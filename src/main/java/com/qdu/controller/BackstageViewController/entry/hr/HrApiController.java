package com.qdu.controller.BackstageViewController.entry.hr;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qdu.codes.BaseConstants;
import com.qdu.codes.CaptchaEnum;
import com.qdu.controller.BackstageViewController.entry.LoginApiController;
import com.qdu.entity.Firm;
import com.qdu.entity.Hr;
import com.qdu.entity.sys.msg.MsgRecord;
import com.qdu.entity.sys.role.Permission;
import com.qdu.entity.sys.role.RolesHr;
import com.qdu.mapper.HrVoMapper;
import com.qdu.service.FirmService;
import com.qdu.service.HrService;
import com.qdu.service.sys.Role.PermissionService;
import com.qdu.service.sys.Role.ResourceService;
import com.qdu.service.sys.Role.RolesHrService;
import com.qdu.service.sys.msg.MsgRecordService;
import com.qdu.util.SendCaptchaUtil;
import com.qdu.vo.HrVO;
import com.qdu.vo.PageBean;
import com.qdu.vo.PageVO;
import com.qdu.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/hr")
@Api(value = "HR管理")
@RequiredArgsConstructor
public class HrApiController {
    private final LoginApiController loginApiController;
    private final MsgRecordService msgRecordService;
    private final HrService hrService;
    private final FirmService firmService;
    private final RolesHrService sysRolesHrService;
    private final PermissionService permissionService;
    private final ResourceService resourceService;
    private final HrVoMapper hrVoMapper;
    private final SendCaptchaUtil captchaUtil;
    @GetMapping("/login")
    @ApiOperation(value = "密码登录")
    public ResultVO<?> login(HttpServletRequest request,
                             String username,
                             String password,
                             int state) {
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            return ResultVO.failure("登录帐号和密码不能为空");
        }
        val hr = new Hr();
        hr.setHname(username).setHpwd(SecureUtil.md5(password));
        val user = hrService.login(hr);
        if (user != null) {
            if (user.getIsLocked()) {
                return ResultVO.failure("该帐号已经被锁定,无法进行登录");
            }
//            用户的资源存session
            setUserResource(request.getSession(), user);
            setLoginCount(user);
                return ResultVO.success("登录成功", user.getHid());
        } else {
            return ResultVO.failure("账户或密码错误!");
        }
    }


    @PostMapping("/send/register")
    @ApiOperation(value = "发送注册验证码")
    public ResultVO<?> sendRegister(@ApiParam(name = "mobile", value = "用户名") String mobile) {
        return loginApiController.sendRegister(mobile);
    }

    @PostMapping("/register")
    @ApiOperation(value = "注册")
    public ResultVO<?> register(HttpServletRequest request,
                                @ApiParam(name = "Hname", value = "用户名") String Hname,
                                @ApiParam(name = "Hnickname", value = "昵称") String Hnickname,
                                @ApiParam(name = "Hpwd", value = "密码") String Hpwd,
                                @ApiParam(name = "Hcontact", value = "手机号") String Hcontact,
                                @ApiParam(name = "Fname", value = "公司名称")  String Fname,
                                @ApiParam(name = "Fdescribe", value = "公司描述") String Fdescribe,
                                @ApiParam(name = "captcha", value = "验证码") String captcha) {
        System.out.println(Hname+" : "+Hnickname+" : "+Hpwd+" : "+Hcontact+" : "+Fname+" : "+Fdescribe+" : "+captcha);
        if (StrUtil.isBlank(Hnickname) ||StrUtil.isBlank(Hname) || StrUtil.isBlank(Hpwd) || StrUtil.isBlank(captcha)) {
            return ResultVO.failure("必填项不能为空");
        }
        val HrWrapper = new QueryWrapper<Hr>();
        LambdaQueryWrapper<Hr> HrQueryWrapper = HrWrapper.lambda();
        HrQueryWrapper.eq(Hr::getHname, Hname);
        val HrtList = hrService.list(HrWrapper);
        if (CollUtil.isNotEmpty(HrtList)) {
            return ResultVO.failure("该手机号已经被注册");
        }
        val wrapper = new QueryWrapper<MsgRecord>();
        LambdaQueryWrapper<MsgRecord> queryWrapper = wrapper.lambda();
        queryWrapper.eq(MsgRecord::getMobile, Hcontact).eq(MsgRecord::getContent, captcha)
                .eq(MsgRecord::getCategory, CaptchaEnum.TEMPLATE_REG.getId())
                .eq(MsgRecord::getResult, true)
                .gt(MsgRecord::getSentTime, DateUtil.offsetMinute(new Date(), -10));
        val msgRecord = msgRecordService.getOne(wrapper);
        if (msgRecord == null) {
            return ResultVO.failure("验证码错误或者已过期,请重新发送验证码");
        }
        val firm = new Firm();
        firm.setFname(Fname)
                .setFdescribe(Fdescribe);
        val Firm = firmService.register(firm);

        val hr = new Hr();
        hr.setHnickname(Hnickname)
                .setHname(Hname)
                .setHpwd(SecureUtil.md5(Hpwd))
                .setHcontact(Hcontact)
                .setFid(Firm.getFid())
        .setHduty("HR");
        val user = hrService.register(hr);
        if (user != null) {
            //用户的资源存session
            setUserResource(request.getSession(), user);
        }
        return ResultVO.success("注册成功");
    }

    @GetMapping("/list")
    @ApiOperation(value = "hr列表")
    public PageVO<HrVO> list(PageBean pageBean) {
        QueryWrapper<HrVO> wrapper = new QueryWrapper<>();
        IPage<HrVO> HrVoPage = hrVoMapper.getHrALL(new Page(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(HrVoPage.getRecords(), HrVoPage.getTotal());
    }
    @GetMapping("/search")
    @ApiOperation(value = "模糊搜索")
    public PageVO<HrVO> searchHRVO(String column,String key, PageBean pageBean) {
            QueryWrapper<HrVO> wrapper = new QueryWrapper<>();
            wrapper.like(column,key) ;//名字包含 o 的
            IPage<HrVO> HrPage = hrVoMapper.getHrALL(new Page(pageBean.getPage(), pageBean.getLimit()), wrapper);
            return PageVO.pageVO(HrPage.getRecords(), HrPage.getTotal());
    }
    @PutMapping("/confirm/power")
    @ApiOperation(value = "hr权限添加")
    public ResultVO<?> power(Hr hr) {
        val rst = hrService.updateById(hr);
        // 添加对应角色
        if (rst) {
            // 删除旧角色
            val wrapper = new QueryWrapper<RolesHr>();
            LambdaQueryWrapper<RolesHr> queryWrapper = wrapper.lambda();
            queryWrapper.eq(RolesHr::getAccountId, hr.getHid());
            sysRolesHrService.remove(wrapper);
            // 添加新角色
            String[] roles = StrUtil.split(hr.getRoles(), ",");
            if (roles != null && roles.length > 0) {
                val RolesHr = new RolesHr();
                RolesHr.setAccountId((long)hr.getHid());
                for (String roleId : roles) {
                    RolesHr.setRoleId(Long.valueOf(roleId));
                    sysRolesHrService.save(RolesHr);
                }
            }
        }
        return rst ? ResultVO.success("更新成功") : ResultVO.success("更新失败");
    }
    @PutMapping("/confirm/edit/locked")
    @ApiOperation(value = "用户锁定状态更改")
    public ResultVO<?> confirmEditLocked(Hr hr) {
        val rst = hrService.updateById(hr);
        return rst ? ResultVO.success("账户已锁定") : ResultVO.success("账户已锁定失败");
    }
    @PutMapping("/register/examine")
    @ApiOperation(value = "Hr注册审批")
    public ResultVO<?> confirmExamine(Long hid,String msg,boolean isLocked) {
        Hr hr=hrService.getById(hid);
        hr.setIsLocked(isLocked);
        if(msg==null){msg=hr.getHname();}
        CaptchaEnum cpt=null;
        Boolean rst = hrService.updateById(hr);
        JSONObject codeJson = JSONUtil.createObj();
        codeJson.set("code", 0000);
        if (isLocked){cpt=CaptchaEnum.TEMPLATE_REJECT;} else {cpt=CaptchaEnum.TEMPLATE_ALLOW;}
         var response= captchaUtil.getCaptcha(hr.getHcontact(), codeJson.toString(), cpt);
        if (!("OK".equals(response.getCode()))) {
            rst = false;
        }

        return rst ? ResultVO.success("发送成功") : ResultVO.success("发送失败");
    }

    @DeleteMapping("/confirm/delete/{id}")
    @ApiOperation(value = "用户删除")
    public ResultVO<?> confirmDelete(@PathVariable Long id) {
        System.out.println("jay!"+id);
        val rst = hrService.removeById(id);
        // 删除角色（权限）
        if (rst) {
            val wrapper = new QueryWrapper<RolesHr>();
            LambdaQueryWrapper<RolesHr> queryWrapper = wrapper.lambda();
            queryWrapper.eq(RolesHr::getAccountId, id);
            sysRolesHrService.remove(wrapper);
        }
        return rst ? ResultVO.success("删除成功") : ResultVO.success("删除失败");
    }
    @DeleteMapping("/confirm/deletes/{ids}")
    @ApiOperation(value = "用户批量删除")
    public ResultVO<?> confirmDeletes(@PathVariable Long ids[]) {
        ResultVO msg = null;
        for (long id : ids) {
            msg = confirmDelete(id);
        }
        return msg;
    }
    private void setUserResource(HttpSession session, Hr hr) {
        session.setAttribute(BaseConstants.USER_SESSION, hr);
        session.setMaxInactiveInterval(BaseConstants.SESSION_TIME_OUT);
        // 用户角色
        val sysRolesAccountList = sysRolesHrService.findByHrId((long)hr.getHid());
        List<Long> roleIdList = new ArrayList<>();
        for (RolesHr rolesHr : sysRolesAccountList) {
            roleIdList.add(rolesHr.getRoleId());
        }
        if (roleIdList.size() == 0) {
            return;
        }
        // 角色权限
        val PermissionList = permissionService.findByRoleIdList(roleIdList);
        List<Long> idList = new ArrayList<>();
        for (Permission sysPermission : PermissionList) {
            idList.add(sysPermission.getSubmenuId());
        }
        if (idList.size() == 0) {
            return;
        }
        // 权限
        val sysResourceList = resourceService.findByIdList(idList);
        session.setAttribute(BaseConstants.USER_RESOURCE_KEY, sysResourceList);
    }
    /**
     * 设置登录次数和时间
     *
     * @param hr 用户
     */
    private void setLoginCount(Hr hr) {
        hr.setLoginCount(hr.getLoginCount() != null ? hr.getLoginCount() + 1 : 1).setLastLoginTime(new Date());
        hrService.updateById(hr);
    }

}
