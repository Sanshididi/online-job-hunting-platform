package com.qdu.controller.BackstageViewController.entry;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qdu.annotation.AddLoginLog;
import com.qdu.codes.CaptchaEnum;
import com.qdu.codes.BaseConstants;
import com.qdu.codes.LogLoginCategoryEnum;
import com.qdu.controller.BackstageViewController.BaseController;
import com.qdu.entity.Users;
import com.qdu.entity.sys.msg.MsgRecord;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.service.UsersService;
import com.qdu.service.sys.msg.MsgRecordService;
import com.qdu.service.sys.sys.AccountService;
import com.qdu.util.SendCaptchaUtil;
import com.qdu.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @author:sanShi
 * @Description:{User登陆注册业务处理}
 * @date :2020/12/1
 */
@RestController
@RequestMapping("/api/entry")
@Api(value = "登录注册模块")
@RequiredArgsConstructor
public class LoginApiController extends BaseController {
    private final AccountService sysAccountService;
    private final UsersService usersService;
    private final MsgRecordService msgRecordService;
    private final SendCaptchaUtil captchaUtil;

    /**
     * 登录
     * User密码登录
     *
     * @return 主页
     */
    @GetMapping("/login")
    @ApiOperation(value = "密码登录")
    public ResultVO<?> login(HttpServletRequest request,
                             String username,
                             String password,
                              int state) {
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            return ResultVO.failure("登录帐号和密码不能为空");
        }
        val users = new Users();
        users.setUtel(username);
        users.setUpwd(SecureUtil.md5(password));
        val user = usersService.login(users);
        if (user != null) {
//            用户的资源存session
            setUserResource(request.getSession(), user);
//           setLoginCount(user);
               return ResultVO.success("登录成功", "/search/findPositionneed");
        } else {
            return ResultVO.failure("账户或密码错误!");
        }
    }
    /**
     * 登录
     * User验证码登录
     *
     * @return 主页
     */
    @GetMapping("/captchaLogin")
    @ApiOperation(value = "验证码登录")
    public ResultVO<?> captchaLogin(HttpServletRequest request,
                                    @ApiParam(name = "captchaLoginUsername", value = "用户名") String captchaLoginUsername,
                                    @ApiParam(name = "captcha", value = "验证码") String captcha) {
        if (StrUtil.isBlank(captchaLoginUsername) || StrUtil.isBlank(captcha)) {
            return ResultVO.failure("登录帐号和验证码不能为空");
        }
        val wrapper = new QueryWrapper<MsgRecord>();
        LambdaQueryWrapper<MsgRecord> queryWrapper = wrapper.lambda();
        queryWrapper.eq(MsgRecord::getMobile, captchaLoginUsername).eq(MsgRecord::getContent, captcha)
                .eq(MsgRecord::getCategory, CaptchaEnum.TEMPLATE_LOGIN.getId())
                .eq(MsgRecord::getResult, true)
                .gt(MsgRecord::getSentTime, DateUtil.offsetMinute(new Date(), -10));
        val msgRecord = msgRecordService.getOne(wrapper);
        if (msgRecord == null) {
            return ResultVO.failure("验证码已过期,请重新发送验证码");
        }
            val users = new Users();
        users.setUtel(captchaLoginUsername);
        val user = usersService.login(users);
        if (user != null) {
            //用户的资源存session
            setUserResource(request.getSession(), user);
            return ResultVO.success("登录成功", "/search/findPositionneed");
        } else {
            return ResultVO.failure("该帐号并没有注册,请先去注册后在登录");
        }
    }
    /**
     * 注册
     * User手机号注册
     *
     * @return 登录页面
     */
    @PostMapping("/register")
    @ApiOperation(value = "注册")
    public ResultVO<?> register(HttpServletRequest request,
                                @ApiParam(name = "registerUsername", value = "用户名") String registerUsername,
                                @ApiParam(name = "password", value = "密码") String password,
                                @ApiParam(name = "captcha", value = "验证码") String captcha) {
        if (StrUtil.isBlank(registerUsername) || StrUtil.isBlank(password) || StrUtil.isBlank(captcha)) {
            return ResultVO.failure("必填项不能为空");
        }
        val sysAccountWrapper = new QueryWrapper<Users>();
        LambdaQueryWrapper<Users> sysAccountQueryWrapper = sysAccountWrapper.lambda();
        sysAccountQueryWrapper.eq(Users::getUtel, registerUsername);
        val sysAccountList = usersService.list(sysAccountWrapper);
        if (CollUtil.isNotEmpty(sysAccountList)) {
            return ResultVO.failure("该手机号已经被注册");
        }
        val wrapper = new QueryWrapper<MsgRecord>();
        LambdaQueryWrapper<MsgRecord> queryWrapper = wrapper.lambda();
        queryWrapper.eq(MsgRecord::getMobile, registerUsername).eq(MsgRecord::getContent, captcha)
                .eq(MsgRecord::getCategory, CaptchaEnum.TEMPLATE_REG.getId())
                .eq(MsgRecord::getResult, true)
                .gt(MsgRecord::getSentTime, DateUtil.offsetMinute(new Date(), -10));
        val msgRecord = msgRecordService.getOne(wrapper);
        if (msgRecord == null) {
            return ResultVO.failure("验证码错误或者已过期,请重新发送验证码");
        }
        val sysAccount = new Users();
        sysAccount.setUtel(registerUsername);
        sysAccount.setUpwd(SecureUtil.md5(password));
        val user = usersService.register(sysAccount);
        if (user != null) {
            //用户的资源存session
            setUserResource(request.getSession(), user);
        }
        return ResultVO.success("注册成功", "/login");
    }

    /**
     * 用户登出
     *
     * @param session session
     * @return rst
     */
    @GetMapping("/logout")
    @ApiOperation(value = "用户登出")
    public ResultVO<?> logout(HttpSession session) {
        session.invalidate();
        return ResultVO.success("退出登录成功");
    }
    /**
     * 发送登录验证码
     *
     *
     * @return 主页
     */
    @PostMapping("/send/captcha")
    @ApiOperation(value = "发送验证码登录")
    public ResultVO<?> sendCaptcha(@ApiParam(name = "mobile", value = "用户名") String mobile) {
        val code = RandomUtil.randomInt(100000, 999999);
        JSONObject codeJson = JSONUtil.createObj();
        codeJson.set("code", code);
        val response = captchaUtil.getCaptcha(mobile, codeJson.toString(), CaptchaEnum.TEMPLATE_LOGIN);
        boolean rst = false;
        if (response != null) {
            val msgRecord = new MsgRecord();
            msgRecord.setCategory(CaptchaEnum.TEMPLATE_LOGIN.getId());
            msgRecord.setCategoryName(CaptchaEnum.TEMPLATE_LOGIN.getName());
            msgRecord.setMobile(mobile);
            msgRecord.setContent(String.valueOf(code));
            msgRecord.setSentTime(new Date());
            boolean result = false;
            if ("OK".equals(response.getCode())) {
                result = true;
            }
            msgRecord.setResult(result);
            msgRecord.setResultDesc(JSONUtil.toJsonStr(response));
            msgRecord.setCreateTime(new Date());
            rst = msgRecordService.save(msgRecord);
            if (!result) {
                return ResultVO.failure("您今日可发短信已到上限!");
            }
        }
        return rst ? ResultVO.success("发送短信成功") : ResultVO.failure("发送短信异常");
    }
    /**
     *
     * 发送注册验证码
     *
     * @return 主页
     */
    @PostMapping("/send/register")
    @ApiOperation(value = "发送注册验证码")
    public ResultVO<?> sendRegister(@ApiParam(name = "mobile", value = "用户名") String mobile) {
        val code = RandomUtil.randomInt(100000, 999999);
        JSONObject codeJson = JSONUtil.createObj();
        codeJson.set("code", code);
        val response = captchaUtil.getCaptcha(mobile, codeJson.toString(), CaptchaEnum.TEMPLATE_REG);
        boolean rst = false;
        if (response != null) {
            val msgRecord = new MsgRecord();
            msgRecord.setCategory(CaptchaEnum.TEMPLATE_REG.getId());
            msgRecord.setCategoryName(CaptchaEnum.TEMPLATE_REG.getName());
            msgRecord.setMobile(mobile);
            msgRecord.setContent(String.valueOf(code));
            msgRecord.setSentTime(new Date());
            boolean result = false;
            if ("OK".equals(response.getCode())) {
                result = true;
            }
            msgRecord.setResult(result);
            msgRecord.setResultDesc(JSONUtil.toJsonStr(response));
            msgRecord.setCreateTime(new Date());
            rst = msgRecordService.save(msgRecord);
            if (!result) {
                return ResultVO.failure("您今日可发短信已到上限!");
            }
        }
        return rst ? ResultVO.success("发送短信成功") : ResultVO.failure("发送短信异常");
    }
    /**
     * 设置登录权限
     *
     * @param session    session
     * @param users 用户
     */
    private void setUserResource(HttpSession session, Users users) {
        session.setAttribute(BaseConstants.USER_SESSION, users);
        session.setMaxInactiveInterval(BaseConstants.SESSION_TIME_OUT);
    }

    /**
     * 设置登录次数和时间
     *
     * @param sysAccount 用户
     */
//    private void setLoginCount(SysAccount sysAccount) {
//        sysAccount.setLoginCount(sysAccount.getLoginCount() != null ? sysAccount.getLoginCount() + 1 : 1).setLastLoginTime(new Date());
//        sysAccountService.updateById(sysAccount);
//    }
}
