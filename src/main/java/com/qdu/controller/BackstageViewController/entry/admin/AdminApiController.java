package com.qdu.controller.BackstageViewController.entry.admin;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.qdu.annotation.AddLoginLog;
import com.qdu.codes.BaseConstants;
import com.qdu.codes.LogLoginCategoryEnum;
import com.qdu.controller.BackstageViewController.BaseController;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.service.sys.sys.AccountService;
import com.qdu.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @author:sanShi
 * @Description:{登陆处理}
 * @date :2020/12/1
 */
@RestController
@RequestMapping("/api/admin/entry")
@Api(value = "登录注册模块")
@RequiredArgsConstructor
public class AdminApiController extends BaseController {
    private final AccountService sysAccountService;
    @GetMapping("/login")
    @ApiOperation(value = "密码登录")
    @AddLoginLog(logLoginCategory = LogLoginCategoryEnum.PASSWORD_LOGIN)
    public ResultVO<?> login(HttpServletRequest request,
                             String username,
                             String password,
                              int state) {
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            return ResultVO.failure("登录帐号和密码不能为空");
        }
        val sysAccount = new SysAccount();
        sysAccount.setCode(username).setPassword(SecureUtil.md5(password));
        val user = sysAccountService.login(sysAccount);
        if (user != null) {
            if (user.getIsLocked()) {
                return ResultVO.failure("该帐号已经被锁定,无法进行登录");
            }
//            用户的资源存session
            setUserResource(request.getSession(), user);
           setLoginCount(user);
           if (state==1){
               return ResultVO.success("登录成功", "/index");
           }else {
               return ResultVO.success("登录成功", "/back_index");
           }
        } else {
            return ResultVO.failure("账户或密码错误!");
        }
    }

    private void setUserResource(HttpSession session, SysAccount sysAccount) {
        session.setAttribute(BaseConstants.USER_SESSION, sysAccount);
        session.setMaxInactiveInterval(BaseConstants.SESSION_TIME_OUT);
//        // 用户角色
//        val sysRolesAccountList = sysRolesAccountService.findByAccountId(sysAccount.getId());
//        List<Long> roleIdList = new ArrayList<>();
//        for (SysRolesAccount sysRolesAccount : sysRolesAccountList) {
//            roleIdList.add(sysRolesAccount.getRoleId());
//        }
//        if (roleIdList.size() == 0) {
//            return;
//        }
//        // 角色权限
//        val sysPermissionList = sysPermissionService.findByRoleIdList(roleIdList);
//        List<Long> idList = new ArrayList<>();
//        for (SysPermission sysPermission : sysPermissionList) {
//            idList.add(sysPermission.getSubmenuId());
//        }
//        if (idList.size() == 0) {
//            return;
//        }
//        // 权限
//        val sysResourceList = sysResourceService.findByIdList(idList);
//        session.setAttribute(BaseConstants.USER_RESOURCE_KEY, sysResourceList);
    }

    /**
     * 设置登录次数和时间
     *
     * @param sysAccount 用户
     */
    private void setLoginCount(SysAccount sysAccount) {
        sysAccount.setLoginCount(sysAccount.getLoginCount() != null ? sysAccount.getLoginCount() + 1 : 1).setLastLoginTime(new Date());
        sysAccountService.updateById(sysAccount);
    }
}
