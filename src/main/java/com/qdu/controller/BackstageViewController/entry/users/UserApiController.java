package com.qdu.controller.BackstageViewController.entry.users;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.entity.sys.role.RolesHr;
import com.qdu.service.sys.sys.AccountService;
import com.qdu.service.sys.Role.RolesHrService;
import com.qdu.vo.PageBean;
import com.qdu.vo.PageVO;
import com.qdu.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.*;

/**
 * @author:sanShi
 * @Description:{用户管理，获取用户信息}
 * @date :2020/12/3
 */
@RestController
@RequestMapping("/api/admin/users/user")
@Api(value = "用户管理")
@RequiredArgsConstructor
public class UserApiController {
    private final AccountService sysAccountService;
    private final RolesHrService sysRolesHrService;

    @GetMapping("/list")
    @ApiOperation(value = "用户列表")
    public PageVO<SysAccount> list(SysAccount sysAccount, PageBean pageBean) {
        val wrapper = new QueryWrapper<SysAccount>();
        if (sysAccount != null && StrUtil.isNotBlank(sysAccount.getCode())) {
            //只有sysAccount有值的时候时才进来
            LambdaQueryWrapper<SysAccount> queryWrapper = wrapper.lambda();
            queryWrapper.eq(SysAccount::getCode, sysAccount.getCode());
        }
        //进行分页操作
        IPage<SysAccount> sysAccountPage = sysAccountService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(sysAccountPage.getRecords(), sysAccountPage.getTotal());
    }
    @GetMapping("/search")
    @ApiOperation(value = "模糊搜索")
    public PageVO<SysAccount> search(String column,String key, PageBean pageBean) {
        val wrapper = new QueryWrapper<SysAccount>();
        if (key !=null) {
            wrapper.like(column,key) ;//名字包含 o 的
        }
        //进行分页操作
        IPage<SysAccount> sysAccountPage = sysAccountService.page(new Page<>(pageBean.getPage(), pageBean.getLimit()), wrapper);
        return PageVO.pageVO(sysAccountPage.getRecords(), sysAccountPage.getTotal());
    }

    @PostMapping("/confirm/add")
    @ApiOperation(value = "用户新增")
    public ResultVO<?> confirmAdd(SysAccount sysAccount) {
        sysAccount.setIsLocked(false);
        val rst = sysAccountService.save(sysAccount);
        return rst ? ResultVO.success("新增成功") : ResultVO.success("新增失败");
    }

    @PutMapping("/confirm/edit")
    @ApiOperation(value = "用户编辑")
    public ResultVO<?> confirmEdit(SysAccount sysAccount) {
        val rst = sysAccountService.updateById(sysAccount);
        System.out.println("收到的数据对象" + sysAccount.getName());
        return rst ? ResultVO.success("更新成功") : ResultVO.success("更新失败");
    }
    @PutMapping("/confirm/edit/locked")
    @ApiOperation(value = "用户锁定状态更改")
    public ResultVO<?> confirmEditLocked(SysAccount sysAccount) {
        val rst = sysAccountService.updateById(sysAccount);
        return rst ? ResultVO.success("账户已锁定") : ResultVO.success("账户已锁定失败");
    }

    @DeleteMapping("/confirm/delete/{id}")
    @ApiOperation(value = "用户删除")
    public ResultVO<?> confirmDelete(@PathVariable Long id) {
        val rst = sysAccountService.removeById(id);
        // 删除角色
        if (rst) {
            val wrapper = new QueryWrapper<RolesHr>();
            LambdaQueryWrapper<RolesHr> queryWrapper = wrapper.lambda();
            queryWrapper.eq(RolesHr::getAccountId, id);
            sysRolesHrService.remove(wrapper);
        }
        return rst ? ResultVO.success("删除成功") : ResultVO.success("删除失败");
    }

    @DeleteMapping("/confirm/deletes/{ids}")
    @ApiOperation(value = "用户批量删除")
    public ResultVO<?> confirmDeletes(@PathVariable Long ids[]) {
        ResultVO msg = null;
        for (long id : ids) {
            msg = confirmDelete(id);
        }
        return msg;
    }
}
