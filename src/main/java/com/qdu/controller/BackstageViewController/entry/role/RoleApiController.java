package com.qdu.controller.BackstageViewController.entry.role;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qdu.controller.BackstageViewController.BaseController;
import com.qdu.entity.sys.role.Permission;
import com.qdu.entity.sys.role.Resource;
import com.qdu.entity.sys.role.Role;
import com.qdu.param.permission.RolePermissionParam;
import com.qdu.service.sys.Role.PermissionService;
import com.qdu.service.sys.Role.ResourceService;
import com.qdu.service.sys.Role.RoleService;
import com.qdu.vo.PageVO;
import com.qdu.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/role/rolePermission")
@Api(value = "角色管理")
@RequiredArgsConstructor
public class RoleApiController extends BaseController {
    private final PermissionService permissionService;
    private final RoleService roleService;
    private final ResourceService resourceService;

    @GetMapping("/list")
    @ApiOperation(value = "获取角色列表数据")
    public PageVO<Role> list() {
        val wrapper = new QueryWrapper<Role>();
        LambdaQueryWrapper<Role> queryWrapper = wrapper.lambda();
        queryWrapper.orderByDesc(Role::getOrdinal);
        List<Role> list = roleService.list(wrapper);
        return PageVO.pageVO(list, (long) list.size());
    }

    @ApiOperation(value = "删除角色")
    @DeleteMapping("/confirm/delete/{id}")
    public ResultVO<?> confirmDelete(@PathVariable Long id) {
        // 删除角色
        val rst = roleService.removeById(id);
        // 删除角色全部权限
        val sysPermissionWrapper = new QueryWrapper<Permission>();
        LambdaQueryWrapper<Permission> sysPermissionQueryWrapper = sysPermissionWrapper.lambda();
        sysPermissionQueryWrapper.eq(Permission::getRoleId, id);
        permissionService.remove(sysPermissionWrapper);
        return rst ? ResultVO.success("删除成功") : ResultVO.failure("删除失败");
    }

    @ApiOperation(value = "删除角色")
    @DeleteMapping("/confirm/deletes/{ids}")
    public ResultVO<?> confirmDeletes(@PathVariable Long[] ids) {
        ResultVO msg = null;
        for (long id : ids) {
            msg = confirmDelete(id);
        }
        return msg;
    }
    @ApiOperation(value = "新增角色")
    @PostMapping("/confirm/add")
    public ResultVO<?> confirmAdd(HttpSession session, Role sysRole,
                                  @ApiParam(name = "rolePermissionStr", value = "角色权限") String rolePermissionStr) {
        JSONArray array = JSONUtil.parseArray(rolePermissionStr);
        List<RolePermissionParam> rolePermissionParamList = new ArrayList<>();
        for (Object object : array) {
            rolePermissionParamList.add(JSONUtil.toBean(object.toString(), RolePermissionParam.class));
        }
        val wrapper = new QueryWrapper<Role>();
        LambdaQueryWrapper<Role> queryWrapper = wrapper.lambda();
        queryWrapper.eq(Role::getName, sysRole.getName());
        val list = roleService.list(wrapper);
        if (CollUtil.isNotEmpty(list)) {
            return ResultVO.failure("该角色已存在");
        }
        sysRole.setCreator(getLoginUser(session).getCode());
        val rst = roleService.save(sysRole);
        if (rst) {
            addRolePermission(sysRole.getId(), getLoginUser(session).getCode(), rolePermissionParamList);
        }
        return rst ? ResultVO.success("新增成功") : ResultVO.failure("新增失败");
    }

    @ApiOperation(value = "编辑角色")
    @PutMapping("/confirm/edit")
    public ResultVO<?> confirmEdit(HttpSession session, Role sysRole,
                                   @ApiParam(name = "rolePermissionStr", value = "角色权限") String rolePermissionStr) {
        // 更改角色信息
        val rst = roleService.updateById(sysRole);
        // 删除角色全部权限
        val sysPermissionWrapper = new QueryWrapper<Permission>();
        LambdaQueryWrapper<Permission> sysPermissionQueryWrapper = sysPermissionWrapper.lambda();
        sysPermissionQueryWrapper.eq(Permission::getRoleId, sysRole.getId());
        permissionService.remove(sysPermissionWrapper);
        // 新增角色权限
        JSONArray array = JSONUtil.parseArray(rolePermissionStr);
        List<RolePermissionParam> rolePermissionParamList = new ArrayList<>();
        for (Object object : array) {
            rolePermissionParamList.add(JSONUtil.toBean(object.toString(), RolePermissionParam.class));
        }
        if (rst) {
            addRolePermission(sysRole.getId(), getLoginUser(session).getCode(), rolePermissionParamList);
        }
        return rst ? ResultVO.success("编辑成功") : ResultVO.failure("编辑失败");
    }
    /**
     * 新增角色权限
     *
     * @param id                      角色id
     * @param creator                 创建人
     * @param rolePermissionParamList 角色集合
     */
    private void addRolePermission(Long id, String creator, List<RolePermissionParam> rolePermissionParamList) {
        val sysPermission = new Permission();
        sysPermission.setRoleId(id);
        for (RolePermissionParam permissionParam : rolePermissionParamList) {
            sysPermission.setSubmenuId(permissionParam.getId());
            sysPermission.setCreator(creator);
            permissionService.save(sysPermission);
            if (CollUtil.isNotEmpty(permissionParam.getChildren())) {
                addRolePermission(id, creator, permissionParam.getChildren());
            }
        }
    }

    @GetMapping("/viewPermission")
    @ApiOperation(value = "根据角色id获取对应的权限树")
    public List<RolePermissionParam> viewPermission(@ApiParam(name = "id", value = "角色id") Long id) {
        List<RolePermissionParam> list = new ArrayList<>();
        List<Long> roleIdList = new ArrayList<>();
        roleIdList.add(id);
        val sysPermissionList = permissionService.findByRoleIdList(roleIdList);
        if (CollUtil.isNotEmpty(sysPermissionList)) {
            List<Long> idList = new ArrayList<>();
            for (Permission sysPermission : sysPermissionList) {
                idList.add(sysPermission.getSubmenuId());
            }
            val sysResourceList = resourceService.findByIdList(idList);
            list = set(sysResourceList, "0", true, null);
        }
        return list;
    }

    @GetMapping("/addPermission")
    @ApiOperation(value = "新增角色时候获取全部权限菜单树")
    public List<RolePermissionParam> addPermission() {
        return set(resourceService.list(), "0", false, null);
    }

    /**
     * 轮训获取对应的权限
     *
     * @param resourceList 权限列表
     * @param superId      父级
     * @param spread       是否展开
     * @param menuIdList   拥有的权限id集合
     * @return 权限列表
     */
    private List<RolePermissionParam> set(List<Resource> resourceList, String superId, Boolean spread, List<Long> menuIdList) {
        val list = new ArrayList<RolePermissionParam>();
        for (Resource sysResource : resourceList) {
            if (superId.equals(sysResource.getSuperId())) {
                val param = new RolePermissionParam();
                param.setTitle(sysResource.getTitle());
                param.setId(sysResource.getId());
                param.setSpread(spread);
                param.setDisabled(false);
                val rolePermissionParams = set(resourceList, sysResource.getId().toString(), spread, menuIdList);
                if (CollUtil.isNotEmpty(rolePermissionParams)) {
                    param.setChildren(rolePermissionParams);
                } else {
                    param.setChildren(new ArrayList<>());
                    // 判断是否属于已经存在
                    param.setChecked(CollUtil.isNotEmpty(menuIdList) && menuIdList.contains(sysResource.getId()));
                }
                list.add(param);
            }
        }
        return list;
    }

    @GetMapping("/editPermission")
    @ApiOperation(value = "编辑角色时候获取角色权限菜单树")
    public List<RolePermissionParam> editPermission(@ApiParam(name = "roleId", value = "角色id") Long roleId) {
        List<Long> roleIdList = new ArrayList<>();
        roleIdList.add(roleId);
        val sysPermissionList = permissionService.findByRoleIdList(roleIdList);
        List<Long> menuIdList = new ArrayList<>();
        for (Permission sysPermission : sysPermissionList) {
            menuIdList.add(sysPermission.getSubmenuId());
        }
        return set(resourceService.list(), "0", false, menuIdList);
    }
}
