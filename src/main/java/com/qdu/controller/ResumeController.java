package com.qdu.controller;


import com.qdu.entity.Resume;
import com.qdu.entity.Users;
import com.qdu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/resume")
public class ResumeController {
    @Autowired
    private ResumeService resumeService;


    @RequestMapping("/to_resume")
    public String toResume() {
        return "resume";
    }

//    @RequestMapping("/to_resumeplus")
//    public String toResumeplus() {
//        return "resumeplus";
//    }


    @GetMapping("/findResumelist")
    public String findResumeslist(HttpSession session,Model model) {

        Users users=(Users) session.getAttribute("userSession");

        model.addAttribute("resumeList", resumeService.getResumeList(users.getUid()));

        return "resumelist";
    }

    @GetMapping("/findResumes")
    public String findResumes(HttpSession session,Model model) {

        Users users=(Users) session.getAttribute("userSession");

        model.addAttribute("resumeList", resumeService.getResumeList(users.getUid()));

        return "resume";
    }

    @PostMapping("/add_resume")
    public String addResume(Resume r) {
        resumeService.addResume(r);
        return "redirect:/resume/findResumes";
    }

    //根据编号查询单个简历信息，然后跳转到resumeplus.jsp页面显示信息，方便编辑
    @GetMapping("/to_resumeplus/{rId}")
    public String toresumeplus(@PathVariable String rId, Model model) {
        model.addAttribute("resume", resumeService.getResumeById(rId));

        return "resumeplus";
    }


    @PostMapping("/edit_resume")
    public String editResume(Resume r) {
        System.out.println("############################################################################################################");
        System.out.println(r.getRid());
        resumeService.updateResume(r);
        return "redirect:/resume/findResumes";
    }
}
