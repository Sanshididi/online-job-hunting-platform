package com.qdu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qdu.entity.Firm;
import com.qdu.entity.Hr;
import com.qdu.service.FirmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Controller
@RequestMapping("/firm")
public class FirmController {

    @Autowired
    private FirmService firmService;

    //测试: 查询公司列表，返回的列表显示在haha.html页面上
    @GetMapping("/findFirms")
    public String findFirms(Model model) {
        model.addAttribute("list",firmService.list());
        return "haha";
    }


    @GetMapping("/edit_firm")
    public String editfrim(Firm firm,int hid){
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("fid",firm.getFid());
        firmService.update(firm,wrapper);
        return "redirect:/hr?hid="+hid;
    }
}

