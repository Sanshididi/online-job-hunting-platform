package com.qdu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.qdu.entity.Positionneed;
import com.qdu.mapper.ApplyVoMapper;
import com.qdu.mapper.HrFirmMapper;
import com.qdu.mapper.PositionVOMapper;
import com.qdu.service.PositionneedService;
import com.qdu.vo.ApplyVO;
import com.qdu.vo.HrVO;
import com.qdu.vo.PositionVO;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Controller
@RequestMapping("/positionneed")
public class PositionneedController {

    @Autowired
    private PositionneedService positionneedService;

    @Autowired
    private ApplyVoMapper applyVoMapper ;

    @Autowired
    private HrFirmMapper hrFirmMapper ;

    @Autowired
    private PositionVOMapper positionVOMapper;

    @GetMapping("/positiondetail")
    public String showPositionDetail(int positionID,int hid ,HttpSession session){
        Positionneed p=positionneedService.getById(positionID);
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("hid",hid);
        List<Positionneed> positionList1=positionneedService.list(wrapper);
        session.setAttribute("position",p);
        session.setAttribute("positionList1",positionList1);

        List<ApplyVO> applyList=applyVoMapper.showApplyList(positionID);
        session.setAttribute("applyList",applyList);
        return "position";
    }

    @GetMapping("/positiondetailuser")
    public String showPositionDetailUser(int positionID,HttpSession session){
        PositionVO positionVO=positionVOMapper.showPositionHrFirmList(positionID);
        session.setAttribute("position",positionVO);
        return  "user_position";
    }

    @PostMapping("/delete")
    @ResponseBody
    public void deleteByPositionID(int positionID){
        positionneedService.removeById(positionID);
    }

    @PostMapping("/update_position")
    public ModelAndView editPositon(Positionneed p) {
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("positionID",p.getPositionID());
        positionneedService.update(p,wrapper);

        int iid=p.getHid();

        return new ModelAndView("redirect:/hr?hid="+iid);
    }

}

