package com.qdu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qdu.entity.Hr;
import com.qdu.entity.Positionneed;
import com.qdu.mapper.HrFirmMapper;
import com.qdu.mapper.HrMapper;
import com.qdu.service.HrService;
import com.qdu.service.PositionneedService;
import com.qdu.vo.HrVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Controller
@RequestMapping("/hr")
public class HrController {

    @Autowired
    private HrService hrService;

    @Autowired
    private HrFirmMapper hrFirmMapper ;

    @Autowired
    private PositionneedService positionneedService;

    //根据招聘人员id查询，返回个人主页hr.html
    @GetMapping
    public String show(int hid, HttpSession session,@RequestParam(defaultValue="1") int pageNo, Model model) {


        HrVO hrvo=hrFirmMapper.findOneDetail(hid);
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("hid",hid);
        PageHelper.startPage(pageNo, 3);
        List<Positionneed> positionList=positionneedService.list(wrapper);
        session.setAttribute("positionList", positionList);
        session.setAttribute("hr", hrvo);


        model.addAttribute("positionList", positionList);
        model.addAttribute("hid", hid);
        PageInfo<Positionneed> pageInfo=new PageInfo<>(positionList,5);
        //将PageInfo对象添加到Model，这样可以在页面上获取所有的分页信息
        model.addAttribute("pageInfo",pageInfo);

        QueryWrapper wrapper1=new QueryWrapper();
        wrapper.eq("hid",hid);
        List<Positionneed> positionList2=positionneedService.list(wrapper1);
        session.setAttribute("positionList2",positionList2);

        return "hr";
    }


    //跳转到add_position.html页面
    @GetMapping("/to_add_position/{hid}")
    public String toAddPosition(@PathVariable int hid,Model model) {
        model.addAttribute("hid",hid);
        return "add_position";
    }

    //添加职位，并以重定向的方式发送/index请求，根据SpringMVCConfig类中配置，/index请求会导致跳转到index.html页面
    @PostMapping("/add_position")
    public String addPosition(Positionneed p) {
        positionneedService.save(p);
        return "redirect:/hr?hid="+p.getHid(); //使用重定向发起一个新请求，跳转到index页面
    }

    //跳转到update_hrAndfirm.html页面
    @GetMapping("/to_update_hrAndfirm/{hid}")
    public String toupdatehrAndfirm(@PathVariable int hid,Model model) {
        model.addAttribute("hid",hid);
        return "update_hrAndfirm";
    }

    @GetMapping("/edit_hr")
    public String edithr(Hr hr){
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("hid",hr.getHid());
        hrService.update(hr,wrapper);
        return "redirect:/hr?hid="+hr.getHid();
    }

    @GetMapping("/to_edit_position/{positionID}")
    public String toeditposition(@PathVariable int positionID,Model model){
        Positionneed p=positionneedService.getById(positionID);
        model.addAttribute("positiondetail",p);
        return "edit_position";
    }


}

