package com.qdu.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PositionVO {
    @TableId(value = "PositionID", type = IdType.AUTO)
    private Integer positionID;                            /**首字母小写*/

    @TableField("Pname")
    private String pname;

    @TableField("Plocation")
    private String plocation;

    @TableField("Psalary")
    private String psalary;

    @TableField("Pdescribe")
    private String pdescribe;

    @TableField("Pnumber")
    private Integer pnumber;

    @TableField("Papplied")
    private Integer papplied;

    @TableField("receiver")
    private Integer receiver;

    @TableField("HID")
    private Integer hid;

    @TableField("Hname")
    private String Hname;

    @TableField("Hpwd")
    private String Hpwd;

    @TableField("Hnickname")
    private String Hnickname;

    @TableField("Hicon")
    private String Hicon;

    @TableField("Hcontact")
    private String Hcontact;

    @TableField("Hemail")
    private String Hemail;

    @TableField("Hgender")
    private String Hgender;

    @TableField("Hduty")
    private String Hduty;

    @TableField("FID")
    private Integer fid;

    @TableField("Fname")
    private String Fname;

    @TableField("Fename")
    private String Fename;

    @TableField("Fdescribe")
    private String Fdescribe;

    @TableField("Ftype")
    private String Ftype;

    @TableField("Findustry")
    private String Findustry;

    @TableField("Flocation")
    private String Flocation;


}
