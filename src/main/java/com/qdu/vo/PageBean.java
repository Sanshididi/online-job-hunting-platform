package com.qdu.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author:sanShi
 * @Description:{设置分页}
 * @date :2020/12/1
 */
@Data
public class PageBean implements Serializable {
    @ApiModelProperty(value = "每页页数")
    private Long page = 1L;
    @ApiModelProperty(value = "每页条数")
    private Long limit = 10L;
}
