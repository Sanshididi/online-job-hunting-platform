package com.qdu.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qdu.entity.Hr;
import lombok.Data;

import java.io.Serializable;

@Data
public class HrVO {
    @TableId(value = "HID", type = IdType.AUTO)
    private Integer hid;

    @TableField("Hname")
    private String Hname;

    @TableField("Hpwd")
    private String Hpwd;

    @TableField("Hnickname")
    private String Hnickname;

    @TableField("Hicon")
    private String Hicon;

    @TableField("Hcontact")
    private String Hcontact;

    @TableField("Hemail")
    private String Hemail;

    @TableField("Hgender")
    private String Hgender;

    @TableField("Hduty")
    private String Hduty;

    @TableField("FID")
    private Integer fid;

    @TableField("Fname")
    private String Fname;

    @TableField("Fename")
    private String Fename;

    @TableField("Fdescribe")
    private String Fdescribe;

    @TableField("Ftype")
    private String Ftype;

    @TableField("Findustry")
    private String Findustry;

    @TableField("Flocation")
    private String Flocation;


}
