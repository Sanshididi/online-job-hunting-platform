package com.qdu.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class ApplyVO {

    @TableId(value = "AID", type = IdType.AUTO)
    private Integer aid;

    @TableField("UID")
    private Integer uid;

    private String uname;

    private String uicon;

    private String ugender;

    private String ubirth;
    /**
     * 用户受教育程度
     */
    private String uedu;
    /**
     * 用户工作经验
     */
    private String uexp;

    private String utel;

    private String uemail;

    @TableField("PosititionID")
    private Integer PosititionID;

    @TableField("Papplied")
    private Integer papplied;

    @TableField("Aisemploy")
    private Integer Aisemploy;
}


