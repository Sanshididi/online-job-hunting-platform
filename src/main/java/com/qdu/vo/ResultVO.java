package com.qdu.vo;

import com.qdu.codes.ErrorEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author:sanShi
 * @Description:{返回错误码}
 * @date :2020/12/1
 * <p>
 * vo:value object,值对象
 * vo就是我们在web的controller层返回的Object，
 * 在接口中这个VO都会被转成Json对象输出，view object
 */
@Data
@ApiModel(value = "返回码")
public class ResultVO<T> implements Serializable {
    @ApiModelProperty(value = "返回编码")
    private String code;
    @ApiModelProperty(value = "返回消息")
    private String msg;
    @ApiModelProperty(value = "返回数据")
    private T data;

    private ResultVO(ErrorEnum errorEnum) {
        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
    }

    private ResultVO(ErrorEnum errorEnum, String msg) {
        this.code = errorEnum.getCode();
        this.msg = msg;
    }

    private ResultVO(ErrorEnum errorEnum, T data) {
        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
        this.data = data;
    }

    private ResultVO(ErrorEnum errorEnum,String msg, T data) {
        this.code = errorEnum.getCode();
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResultVO<T> success() {
        return new ResultVO<>(ErrorEnum.SUCCESS);
    }

    public static <T> ResultVO<T> success(T data) {
        return new ResultVO<>(ErrorEnum.SUCCESS, data);
    }

    public static <T> ResultVO<T> success(String msg) {
        return new ResultVO<>(ErrorEnum.SUCCESS, msg);
    }

    public static <T> ResultVO<T> success(String msg, T data) {
        return new ResultVO<>(ErrorEnum.SUCCESS, msg, data);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum) {
        return new ResultVO<>(errorEnum);
    }

    public static <T> ResultVO<T> failure(String msg) {
        return new ResultVO<>(ErrorEnum.OPERATION_FAILED, msg);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum, String msg) {
        return new ResultVO<>(errorEnum, msg);
    }

    public static <T> ResultVO<T> failure(ErrorEnum errorEnum, T data) {
        return new ResultVO<>(errorEnum, data);
    }

}