package com.qdu.util;

import cn.hutool.json.JSONUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.qdu.codes.CaptchaEnum;
import com.qdu.param.captcha.CaptchaParam;
import com.qdu.param.captcha.CaptchaResponse;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository("CaptchaUtil")
public class SendCaptchaUtil {
    @Value("自行申请密钥")
    private String accessKeyId;
    @Value("自行申请密钥")
    private String accessSecret;

    /**
     * 发送短信验证码
     *
     * @param mobile                    手机号
     * @param code                      验证码
     * @param verificationCodeParamEnum 类型
     * @return 结果
     */
    public CaptchaResponse getCaptcha(String mobile, String code, CaptchaEnum verificationCodeParamEnum) {
        val param = new CaptchaParam();
        param.setPhoneNumbers(mobile);
        param.setSignName(verificationCodeParamEnum.getSignName());
        param.setTemplateCode(verificationCodeParamEnum.getTemplateCode());
        param.setTemplateParam(code);
        return sendSms(param);
    }

    /**
     * 发送验证码
     *
     * @param param 参数
     * @return 结果
     */
    private CaptchaResponse sendSms(CaptchaParam param) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", param.getPhoneNumbers());
        request.putQueryParameter("SignName", param.getSignName());
        request.putQueryParameter("TemplateCode", param.getTemplateCode());
        request.putQueryParameter("TemplateParam", param.getTemplateParam());
        try {
            CommonResponse response = client.getCommonResponse(request);
            return JSONUtil.toBean(response.getData(), CaptchaResponse.class);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

}
