package com.qdu.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.Firm;
import com.qdu.mapper.FirmMapper;
import com.qdu.service.FirmService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Service
@RequiredArgsConstructor
public class FirmServiceImpl extends ServiceImpl<FirmMapper, Firm> implements FirmService {
    private final FirmMapper firmMapper;

    @Override
    public Firm register(Firm firm) {
        val rst =firmMapper.insert(firm);
        if (rst > 0) {
            return firmMapper.selectById(firm.getFid());
        }
        return null;
    }
}
