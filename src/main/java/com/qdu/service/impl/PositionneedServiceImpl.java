package com.qdu.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.Positionneed;
import com.qdu.mapper.PositionneedMapper;
import com.qdu.service.PositionneedService;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Service
public class PositionneedServiceImpl extends ServiceImpl<PositionneedMapper, Positionneed> implements PositionneedService {

}
