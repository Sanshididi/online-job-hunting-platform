package com.qdu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.mapper.HrVoMapper;
import com.qdu.service.HrVoService;
import com.qdu.vo.HrVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author:sanShi
 * @Description:{Hrvo对象服务}
 * @date :2020/12/17
 */
@Service
@RequiredArgsConstructor
public class HrVoServiceImpl extends ServiceImpl<HrVoMapper, HrVO> implements HrVoService {
}
