package com.qdu.service.impl;

import com.qdu.entity.Resume;
import com.qdu.mapper.ResumeMapper;
import com.qdu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Service
@Transactional
public class ResumeServiceImpl implements ResumeService {


    @Autowired
    private  ResumeMapper resumeMapper;

    @Transactional(readOnly=true)
    @Override
    public Resume getResumeById(String rId) {
        return resumeMapper.getOneById(rId);
    }

    @Override
    public void addResume(Resume resume) {
        resumeMapper.insert(resume);
    }

    @Override
    public void updateResume(Resume resume) {
        resumeMapper.updateByPrimaryKey(resume);
    }
    @Transactional(readOnly=true)
    @Override
    public List<Resume> getResumeList(Integer uid) {
        return resumeMapper.getAll(uid);
    }

}
