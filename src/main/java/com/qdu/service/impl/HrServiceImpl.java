package com.qdu.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.Hr;
import com.qdu.entity.Users;
import com.qdu.mapper.FirmMapper;
import com.qdu.mapper.HrMapper;
import com.qdu.service.HrService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Service
@RequiredArgsConstructor
public class HrServiceImpl extends ServiceImpl<HrMapper, Hr> implements HrService {
    private final HrMapper hrMapper;

    @Override
    public Hr login(Hr hr) {
        LambdaQueryWrapper<Hr> wrapper = new LambdaQueryWrapper<>();
        if (hr != null) {
            if (StrUtil.isNotBlank(hr.getHname())) {
                wrapper.eq(Hr::getHname,hr.getHname());
            }
            if (StrUtil.isNotBlank(hr.getHpwd())) {
                wrapper.eq(Hr::getHpwd, hr.getHpwd());
            }
        }
        return hrMapper.selectOne(wrapper);
    }

    @Override
    public Hr register(Hr hr) {
        val rst =hrMapper.insert(hr);
        if (rst > 0) {
            return hrMapper.selectById(hr.getFid());
        }
        return null;
    }
}
