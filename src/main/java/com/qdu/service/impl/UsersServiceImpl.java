package com.qdu.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.Hr;
import com.qdu.entity.Users;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.mapper.HrMapper;
import com.qdu.mapper.UsersMapper;
import com.qdu.service.UsersService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

/**
 * @author:sanShi
 * @Description:{Users实例对象}
 * @date :2020/12/19
 */
@Service
@RequiredArgsConstructor
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {
    private final UsersMapper usMapper;

    /**
     * 登录
     *
     * @param users 用户
     * @return 结果
     */
    @Override
    public Users login(Users users) {
        LambdaQueryWrapper<Users> wrapper = new LambdaQueryWrapper<>();
        if (users != null) {
            if (StrUtil.isNotBlank(users.getUtel())) {
                wrapper.eq(Users::getUtel, users.getUtel());
            }
            if (StrUtil.isNotBlank(users.getUpwd())) {
                wrapper.eq(Users::getUpwd, users.getUpwd());
            }
        }
        return usMapper.selectOne(wrapper);
    }

    @Override
    public Users register(Users users) {
        val rst =usMapper.insert(users);
        if (rst > 0) {
            return usMapper.selectById(users.getUid());
        }
        return null;
    }
}
