package com.qdu.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.Apply;
import com.qdu.mapper.ApplyMapper;
import com.qdu.service.ApplyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Service
public class ApplyServiceImpl extends ServiceImpl<ApplyMapper, Apply> implements ApplyService {

}
