package com.qdu.service.impl;

import com.qdu.entity.Inform;
import com.qdu.entity.Resumestate;
import com.qdu.mapper.InformMapper;
import com.qdu.mapper.ResumestateMapper;
import com.qdu.service.InformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InformServiceImpl implements InformService {

    @Autowired
    private InformMapper informMapper;

    @Autowired
    private ResumestateMapper resumestateMapper;

    @Transactional(readOnly=true)
    @Override
    public List<Inform> getInformListByReceiver(String receiver) {
        System.out.println("serviceimpl:"+informMapper.selectByReceiver(receiver));
        return  informMapper.selectByReceiver(receiver);
    }


    @Override
    public void deleteInform(String informid) {
        informMapper.deleteByPrimaryKey(informid);
    }

    @Override
    public void addResumeState(Resumestate resumestate) {

        System.out.println("添加resumestate");
        resumestateMapper.insert(resumestate);
    }

    @Override
    public void updateResumeStatus(Integer positionID,String receiver,String status) {
        System.out.println("修改resumestate的status");
        resumestateMapper.updateByStatus(positionID,receiver,status);
    }

    @Transactional(readOnly=true)
    @Override
    public List<Resumestate> getRsListByReceiver(String receiver) {
        return resumestateMapper.selectByReceiver(receiver);
    }

    @Override
    public void addInform(Inform inform) {
        informMapper.insert(inform);
    }


}
