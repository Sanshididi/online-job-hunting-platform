package com.qdu.service.impl;

import com.qdu.entity.Positionneed;
import com.qdu.entity.Resume;
import com.qdu.mapper.ResumeMapper;
import com.qdu.mapper.SearchMapper;
import com.qdu.service.ResumeService;
import com.qdu.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Service
@Transactional
public class SearchServiceImpl implements SearchService {


    @Autowired
    private SearchMapper searchMapper;

    @Transactional(readOnly=true)
    @Override
    public Positionneed getPositionneedById(String pId) {
        return searchMapper.getOneById(pId);
    }


    @Transactional(readOnly=true)
    @Override
    public List<Positionneed> getPositionneedList() {
        return searchMapper.getAll();
    }

    @Transactional(readOnly=true)
    @Override
    public List<Positionneed> getPositionneedBypname(String pname) {
        return searchMapper.getByBatchName(pname);
    }

}
