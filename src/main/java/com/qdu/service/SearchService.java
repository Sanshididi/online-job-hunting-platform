package com.qdu.service;

import com.qdu.entity.Positionneed;


import java.util.List;

public interface SearchService {
    Positionneed getPositionneedById(String pId);

    List<Positionneed> getPositionneedBypname(String pname);

    List<Positionneed> getPositionneedList();
}
