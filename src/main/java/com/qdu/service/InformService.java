package com.qdu.service;

import com.qdu.entity.Inform;
import com.qdu.entity.Resumestate;

import java.util.List;

public interface InformService {
    /**
     * <p>
     *  站内系统通知
     * </p>
     *
     * @author Zom
     * @since 2020-12-13
     */
    List<Inform> getInformListByReceiver(String receiver);

    void deleteInform(String informid);

    /**添加投递状态*/
    void addResumeState(Resumestate resumestate);

    void updateResumeStatus(Integer positionID,String receiver,String status);

    /**将Resumestate列表展示在用户个人中心*/
    List<Resumestate> getRsListByReceiver(String receiver);

    /**添加通知*/
    void addInform(Inform inform);
}
