package com.qdu.service.sys.Role.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.role.Resource;
import com.qdu.mapper.sys.role.ResourceMapper;
import com.qdu.service.sys.Role.ResourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements ResourceService {
    private final ResourceMapper resourceMapper;

    @Override
    public List<Resource> findByIdList(List<Long> idList) {
        LambdaQueryWrapper<Resource> wrapper = new LambdaQueryWrapper<>();
        if (CollUtil.isNotEmpty(idList)) {
            wrapper.in(Resource::getId, idList);
        }
        return resourceMapper.selectList(wrapper);
    }
}
