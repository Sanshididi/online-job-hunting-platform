package com.qdu.service.sys.Role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.role.Role;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/31
 */
public interface RoleService extends IService<Role> {
}
