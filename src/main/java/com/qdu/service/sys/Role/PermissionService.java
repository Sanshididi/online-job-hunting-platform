package com.qdu.service.sys.Role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.role.Permission;

import java.util.List;

public interface PermissionService extends IService<Permission> {
    /**
     * 根据角色列表查询对应的权限
     *
     * @param roleIdList 角色列表
     * @return 权限
     */
    List<Permission> findByRoleIdList(List<Long> roleIdList);
}
