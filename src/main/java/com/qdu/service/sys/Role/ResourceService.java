package com.qdu.service.sys.Role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.role.Resource;

import java.util.List;

public interface ResourceService extends IService<Resource> {
    /**
     * 根据ids查询
     *
     * @param idList ids
     * @return 角色权限
     */
    List<Resource> findByIdList(List<Long> idList);
}
