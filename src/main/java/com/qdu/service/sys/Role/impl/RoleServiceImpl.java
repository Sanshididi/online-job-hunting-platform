package com.qdu.service.sys.Role.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.role.Role;
import com.qdu.mapper.sys.role.RoleMapper;
import com.qdu.service.sys.Role.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>implements RoleService {

}
