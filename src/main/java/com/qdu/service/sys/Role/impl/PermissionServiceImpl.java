package com.qdu.service.sys.Role.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.role.Permission;
import com.qdu.mapper.sys.role.PermissionMapper;
import com.qdu.service.sys.Role.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {
    private final PermissionMapper permissionMapper;

    /**
     * 根据角色列表查询对应的权限
     *
     * @param roleIdList 角色列表
     * @return 对应的权限
     */
    @Override
    public List<Permission> findByRoleIdList(List<Long> roleIdList) {
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<>();
        if (CollUtil.isNotEmpty(roleIdList)) {
            wrapper.in(Permission::getRoleId, roleIdList);
        }
        return permissionMapper.selectList(wrapper);
    }
}
