package com.qdu.service.sys.Role.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.role.RolesHr;
import com.qdu.mapper.sys.role.RolesHrMapper;
import com.qdu.service.sys.Role.RolesHrService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/12
 */
@Service
@RequiredArgsConstructor
public class RolesHrServiceImpl extends ServiceImpl<RolesHrMapper, RolesHr> implements RolesHrService {
    private final RolesHrMapper rolesHrMapper;

    /**
     * 根据用户id查询对应角色
     *
     * @param hrId 用户id
     * @return 对应角色
     */
    @Override
    public List<RolesHr> findByHrId(Long hrId) {
        LambdaQueryWrapper<RolesHr> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RolesHr::getAccountId, hrId);
        return rolesHrMapper.selectList(wrapper);
    }

}
