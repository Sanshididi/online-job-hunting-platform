package com.qdu.service.sys.Role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.role.RolesHr;


import java.util.List;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/12
 */
public interface RolesHrService extends IService<RolesHr> {

    /**
     * 根据用户id查询对应角色
     *
     * @param hrId 用户id
     * @return 对应角色
     */
    List<RolesHr> findByHrId(Long hrId);
}
