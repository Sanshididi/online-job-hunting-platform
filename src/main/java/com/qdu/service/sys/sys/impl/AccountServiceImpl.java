package com.qdu.service.sys.sys.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.sys.SysAccount;
import com.qdu.mapper.sys.sys.SysAccountMapper;
import com.qdu.service.sys.sys.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

/**
 * @author Sanshi
 */
@Service
@RequiredArgsConstructor
public class AccountServiceImpl extends ServiceImpl<SysAccountMapper, SysAccount> implements AccountService {
    private final SysAccountMapper sysAccountMapper;



    /**
     * 登录
     *
     * @param sysAccount 用户
     * @return 结果
     */
    @Override
    public SysAccount login(SysAccount sysAccount) {
        LambdaQueryWrapper<SysAccount> wrapper = new LambdaQueryWrapper<>();
        if (sysAccount != null) {
            if (StrUtil.isNotBlank(sysAccount.getCode())) {
                wrapper.eq(SysAccount::getCode, sysAccount.getCode());
            }
            if (StrUtil.isNotBlank(sysAccount.getPassword())) {
                wrapper.eq(SysAccount::getPassword, sysAccount.getPassword());
            }
        }
        return sysAccountMapper.selectOne(wrapper);
    }

    /**
     * 注册
     *
     * @param sysAccount 用户
     * @return 结果
     */
    @Override
    public SysAccount register(SysAccount sysAccount) {
        val rst = sysAccountMapper.insert(sysAccount);
        if (rst > 0) {
            return sysAccountMapper.selectById(sysAccount.getId());
        }
        return null;
    }
}
