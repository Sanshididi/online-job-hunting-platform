package com.qdu.service.sys.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.sys.SysAccount;

/**
 * @author:sanShi
 * @Description:{系统管理员账户}
 * @date :2020/11/30
 */
public  interface AccountService extends IService<SysAccount> {
    /**
     * 登录
     *
     * @param sysAccount 用户
     * @return SysAccount
     */
    SysAccount login(SysAccount sysAccount);

    /**
     * 注册
     *
     * @param sysAccount 用户
     * @return SysAccount
     */
    SysAccount register(SysAccount sysAccount);
}
