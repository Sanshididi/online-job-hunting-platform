package com.qdu.service.sys.log.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.log.LogLogin;
import com.qdu.mapper.sys.log.LogLoginMapper;
import com.qdu.service.sys.log.LogLoginService;
import org.springframework.stereotype.Service;

@Service
public class LogLoginServiceImpl extends ServiceImpl<LogLoginMapper, LogLogin> implements LogLoginService {

}
