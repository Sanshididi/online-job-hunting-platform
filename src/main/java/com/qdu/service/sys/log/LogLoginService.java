package com.qdu.service.sys.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.log.LogLogin;

public interface LogLoginService extends IService<LogLogin> {
}
