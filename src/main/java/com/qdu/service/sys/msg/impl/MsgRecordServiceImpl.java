package com.qdu.service.sys.msg.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qdu.entity.sys.msg.MsgRecord;
import com.qdu.mapper.sys.msg.MsgRecordMapper;
import com.qdu.service.sys.msg.MsgRecordService;
import org.springframework.stereotype.Service;

@Service
public class MsgRecordServiceImpl extends ServiceImpl<MsgRecordMapper, MsgRecord> implements MsgRecordService {
}
