package com.qdu.service.sys.msg;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.sys.msg.MsgRecord;

public interface MsgRecordService extends IService<MsgRecord> {
}
