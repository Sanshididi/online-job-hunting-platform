package com.qdu.service;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.Firm;
import com.qdu.entity.Hr;
import com.qdu.entity.Users;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface HrService extends IService<Hr> {
    /**
     * 登录
     *
     * @param hr 用户
     * @return hr
     */
    Hr login(Hr hr);
    /**
     * 注册
     *
     * @param hr 用户
     * @return Hr
     */
    Hr register(Hr hr);

}
