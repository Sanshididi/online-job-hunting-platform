package com.qdu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.Users;
import com.qdu.entity.sys.sys.SysAccount;

/**
 * @author:sanShi
 * @Description:{Users实例对象}
 * @date :2020/12/19
 */
public interface UsersService extends IService<Users> {
    /**
     * 登录
     *
     * @param users 用户
     * @return users
     */
    Users login(Users users);
    /**
     * 注册
     *
     * @param users 用户
     * @return  users
     */
     Users register(Users users);
}
