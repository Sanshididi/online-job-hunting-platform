package com.qdu.service;

import com.qdu.entity.Resume;

import java.util.List;

public interface ResumeService {

    Resume getResumeById(String rId);
    void addResume(Resume resume);

    void updateResume(Resume resume);
    List<Resume> getResumeList(Integer uid);

}
