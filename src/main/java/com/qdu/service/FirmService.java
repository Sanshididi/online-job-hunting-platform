package com.qdu.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.Firm;
import com.qdu.entity.sys.sys.SysAccount;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface FirmService extends IService<Firm> {
    /**
     * 注册
     *
     * @param frim 用户
     * @return Firm
     */
    Firm register(Firm frim);
}
