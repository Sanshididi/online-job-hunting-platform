package com.qdu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.vo.HrVO;
/**
 * @author:sanShi
 * @Description:{Hrvo}
 * @date :2020/12/17
 */
public interface HrVoService extends IService<HrVO> {
}
