package com.qdu.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qdu.entity.Apply;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface ApplyService extends IService<Apply> {

}
