package com.qdu.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.qdu.codes.BaseConstants;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * @author:sanShi
 * @Description:{Mybatis-puls 自动填充
 * 配置后：下列属性自动插入的数据库（entity类中必须指明填充策略）}
 * @date :2020/12/14
 * */
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        //默认未删除
        setFieldValByName("isDeleted", BaseConstants.NOT_DELETE_FLAG, metaObject);
        //创建时间默认当前时间
        setFieldValByName("createTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //修改时间
        setFieldValByName("modifyTime", new Date(), metaObject);
    }
}
