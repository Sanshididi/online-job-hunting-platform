package com.qdu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//很多配置spring boot会自动帮你配置
//如果还是需要自己配置一些spring mvc的东西，可以创建一个spring mvc配置类
@Configuration
public class SpringMvcConfig implements WebMvcConfigurer{
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//如果上传文件到指定的一个目录，那么可以在这里配置对这个目录下资源的访问
		//这里表示对C:/upload目录下文件的访问需要使用uploadImg前缀
		registry.addResourceHandler("/uploadImg/**")
		        .addResourceLocations("file:D:/upload/");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		//如果只是想实现简单跳转到templates下面的thymeleaf页面
		//可以在addViewController()方法中配置
		registry.addViewController("/fileUpload").setViewName("file_upload");
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/index").setViewName("index");
		registry.addViewController("/login").setViewName("Ulogin");
		registry.addViewController("/login.html").setViewName("Ulogin");
		registry.addViewController("/register.html").setViewName("Ulogin");
		registry.addViewController("/hrlogin").setViewName("Hrlogin");
		registry.addViewController("/back_login").setViewName("back-stage-management/login");
		registry.addViewController("/back_index").setViewName("back-stage-management/index");
		registry.addViewController("/personalcenter").setViewName("personalcenter");
	}
	
    //使用@Bean注册一个spring管理的bean，方法名是bean的id，也就是名称
    @Bean
    public CommonsMultipartResolver multipartResolver() {
    	CommonsMultipartResolver resolver=new CommonsMultipartResolver();
    	//可以通过该bean配置文件上传的一些设置
    	//setMaxUploadSize()用于设置上传的文件大小限制，单位是字节
    	resolver.setMaxUploadSize(10*1024*1024); //这里限制为3M
    	resolver.setDefaultEncoding("UTF-8"); //设置解析请求的编码
    	return resolver;
    }
}
