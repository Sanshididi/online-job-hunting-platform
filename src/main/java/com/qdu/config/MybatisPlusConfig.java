package com.qdu.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author:sanShi
 * @Description:{Mybatis-puls 配置类 PaginationInterceptor指定策略，使用后Ipage正常使用}
 * @date :2020/12/14
 * */

@Configuration
public class MybatisPlusConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        // 分页插件
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // 你的最大单页限制数量，默认 500 条，小于 0 如 -1 不受限制
        paginationInterceptor.setLimit(100);
        return paginationInterceptor;
    }

    /*@BeanIMsgMessageService
    public PerformanceInterceptor performanceInterceptor() {
        // SQL执行效率插件(开发环境使用)
        return new PerformanceInterceptor();
    }*/
}
