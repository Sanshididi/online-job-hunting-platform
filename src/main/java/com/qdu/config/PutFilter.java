package com.qdu.config;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.FormContentFilter;

/**
 * @author:sanShi
 * @Description:{设置put请求的值}
 * @date :2020/12/14
 * */
@Component
public class PutFilter extends FormContentFilter {
}
