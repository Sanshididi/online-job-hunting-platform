package com.qdu.annotation;

import com.qdu.codes.LogLoginCategoryEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author:sanShi
 * @Description:{自定义日志注解}
 * @date :2020/12/14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AddLoginLog {

    LogLoginCategoryEnum logLoginCategory();
}
