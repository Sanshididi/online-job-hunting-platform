package com.qdu.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * resume
 * @author 
 */
@Data
public class Resume implements Serializable {
    /**
     * 简历编号，主键，自动增长
     */
    private Integer rid;

    /**
     * 简历人姓名
     */
    private String rname;

    /**
     * 简历人性别
     */
    private String rgender;

    /**
     * 简历图片
     */
    private String rimg;

    /**
     * 面试人教育水平
     */
    private String redu;

    /**
     * 简历人电话号码
     */
    private String rtel;

    /**
     * 接受面试通知的邮箱
     */
    private String remail;

    /**
     * 期望职位
     */
    private String rpositionDesired;

    /**
     * 期望工作地点
     */
    private String rlocation;

    /**
     * 薪资期望
     */
    private Integer rsalary;

    /**
     * 工作经历
     */
    private String rworkExperience;

    /**
     * 项目经历
     */
    private String rprojectExperience;

    /**
     * 面试人大学
     */
    private String rcollege;

    /**
     * 面试人专业
     */
    private String rpro;

    private Integer uid;


    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * 自我描述
     */

    private String rpersonadvantage;

    private static final long serialVersionUID = 1L;

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public String getRgender() {
        return rgender;
    }

    public void setRgender(String rgender) {
        this.rgender = rgender;
    }

    public String getRimg() {
        return rimg;
    }

    public void setRimg(String rimg) {
        this.rimg = rimg;
    }

    public String getRedu() {
        return redu;
    }

    public void setRedu(String redu) {
        this.redu = redu;
    }

    public String getRtel() {
        return rtel;
    }

    public void setRtel(String rtel) {
        this.rtel = rtel;
    }

    public String getRemail() {
        return remail;
    }

    public void setRemail(String remail) {
        this.remail = remail;
    }

    public String getRpositionDesired() {
        return rpositionDesired;
    }

    public void setRpositionDesired(String rpositionDesired) {
        this.rpositionDesired = rpositionDesired;
    }

    public String getRlocation() {
        return rlocation;
    }

    public void setRlocation(String rlocation) {
        this.rlocation = rlocation;
    }

    public Integer getRsalary() {
        return rsalary;
    }

    public void setRsalary(Integer rsalary) {
        this.rsalary = rsalary;
    }

    public String getRworkExperience() {
        return rworkExperience;
    }

    public void setRworkExperience(String rworkExperience) {
        this.rworkExperience = rworkExperience;
    }

    public String getRprojectExperience() {
        return rprojectExperience;
    }

    public void setRprojectExperience(String rprojectExperience) {
        this.rprojectExperience = rprojectExperience;
    }

    public String getRcollege() {
        return rcollege;
    }

    public void setRcollege(String rcollege) {
        this.rcollege = rcollege;
    }

    public String getRpro() {
        return rpro;
    }

    public void setRpro(String rpro) {
        this.rpro = rpro;
    }

    public String getRpersonadvantage() {
        return rpersonadvantage;
    }

    public void setRpersonadvantage(String rpersonadvantage) {
        this.rpersonadvantage = rpersonadvantage;
    }

    public Resume() {
    }

    public Resume(Integer rid, String rname, String rgender, String rimg, String redu, String rtel, String remail, String rpositionDesired, String rlocation, Integer rsalary, String rworkExperience, String rprojectExperience, String rcollege, String rpro, String rpersonadvantage) {
        this.rid = rid;
        this.rname = rname;
        this.rgender = rgender;
        this.rimg = rimg;
        this.redu = redu;
        this.rtel = rtel;
        this.remail = remail;
        this.rpositionDesired = rpositionDesired;
        this.rlocation = rlocation;
        this.rsalary = rsalary;
        this.rworkExperience = rworkExperience;
        this.rprojectExperience = rprojectExperience;
        this.rcollege = rcollege;
        this.rpro = rpro;
        this.rpersonadvantage = rpersonadvantage;
    }

    public Resume(Integer rid, Integer uid, String rname, String rgender, String rimg, String redu, String rtel, String remail, String rpositionDesired, String rlocation, Integer rsalary, String rworkExperience, String rprojectExperience, String rcollege, String rpro, String rpersonadvantage) {
        this.rid = rid;
        this.uid = uid;
        this.rname = rname;
        this.rgender = rgender;
        this.rimg = rimg;
        this.redu = redu;
        this.rtel = rtel;
        this.remail = remail;
        this.rpositionDesired = rpositionDesired;
        this.rlocation = rlocation;
        this.rsalary = rsalary;
        this.rworkExperience = rworkExperience;
        this.rprojectExperience = rprojectExperience;
        this.rcollege = rcollege;
        this.rpro = rpro;
        this.rpersonadvantage = rpersonadvantage;
    }
}