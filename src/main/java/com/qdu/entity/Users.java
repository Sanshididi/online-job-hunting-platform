package com.qdu.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * users
 * @author 
 */
@Data
public class Users implements Serializable {
    /**
     * 用户编号，主键，自动增长
     */
    private Integer uid;

    /**
     * 用户名
     */
    private String uname;

    /**
     * 用户密码
     */
    private String upwd;

    /**
     * 密码查询问题
     */
    private String uquestion;

    /**
     * 用户头像
     */
    private String uicon;

    /**
     * 用户性别
     */
    private String ugender;

    /**
     * 用户生日
     */
    private String ubirth;

    /**
     * 用户受教育程度
     */
    private String uedu;

    /**
     * 用户工作经验
     */
    private String uexp;

    /**
     * 密码查询答案
     */
    private String uanswer;

    /**
     * 用户职务默认为求职者
     */
    private String hduty;

    /**
     * 用户电话
     */
    private String utel;

    /**
     * 用户电子邮箱
     */
    private String uemail;

    private static final long serialVersionUID = 1L;
}