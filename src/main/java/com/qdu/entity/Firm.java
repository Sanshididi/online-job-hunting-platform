package com.qdu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qdu.entity.sys.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
public class Firm extends BaseEntityNOSU implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "FID", type = IdType.AUTO)
      private Integer fid;

    @TableField("Fname")
    private String Fname;

    @TableField("Fename")
    private String Fename;

    @TableField("Fdescribe")
    private String Fdescribe;

    @TableField("Ftype")
    private String Ftype;

    @TableField("Findustry")
    private String Findustry;

    @TableField("Flocation")
    private String Flocation;


}
