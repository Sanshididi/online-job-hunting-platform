package com.qdu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
public class Apply implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "AID", type = IdType.AUTO)
      private Integer aid;

    @TableField("UID")
    private Integer uid;

    @TableField("PosititionID")
    private Integer PosititionID;

    @TableField("Aisemploy")
    private Integer Aisemploy;


}
