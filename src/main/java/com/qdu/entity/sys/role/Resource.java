package com.qdu.entity.sys.role;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Resource {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long category;
    private String superId;
    private String title;
    private String icon;
    private String href;
    private String target;
    private Long ordinal;
}
