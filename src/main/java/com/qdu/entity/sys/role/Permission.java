package com.qdu.entity.sys.role;

import com.qdu.entity.sys.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Permission extends BaseEntity {
    private Long roleId;
    private Long submenuId;
    private String note;
}
