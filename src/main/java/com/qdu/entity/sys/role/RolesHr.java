package com.qdu.entity.sys.role;

import com.qdu.entity.sys.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/31
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class RolesHr extends BaseEntity {
    private Long accountId;
    private Long roleId;
    private String note;
}
