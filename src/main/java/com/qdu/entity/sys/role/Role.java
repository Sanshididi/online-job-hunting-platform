package com.qdu.entity.sys.role;

import com.baomidou.mybatisplus.annotation.TableField;
import com.qdu.entity.sys.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/31
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseEntity {
    @ApiModelProperty(value = "角色名")
    private String name;
    @ApiModelProperty(value = "排序号")
    private Long ordinal;
    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "是否选中")
    @TableField(exist = false)
    private Boolean isChecked;
}
