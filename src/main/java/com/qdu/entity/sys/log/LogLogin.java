package com.qdu.entity.sys.log;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author:Sanshi
 * @Description:{登录日志实体类}
 * @date :2020/12/14
 */
@Data
@Accessors(chain = true)
public class LogLogin {
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 用户id
     */
    private Long accountId;

    /**
     * 帐号
     */
    private String code;

    /**
     * 昵称
     */
    private String name;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 分类id
     */
    private Long categoryId;

    /**
     * 分类name
     */
    private String categoryName;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Boolean isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}