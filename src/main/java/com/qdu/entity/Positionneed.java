package com.qdu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
@TableName(value = "positionneed")
public class Positionneed implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "PositionID", type = IdType.AUTO)
      private Integer positionID;                            /**首字母小写*/

    @TableField("HID")
    private Integer hid;

    @TableField("Pname")
    private String pname;

    @TableField("Plocation")
    private String plocation;

    @TableField("Psalary")
    private String psalary;

    @TableField("Pdescribe")
    private String pdescribe;

    @TableField("Pnumber")
    private Integer pnumber;

    @TableField("Papplied")
    private Integer papplied;

    @TableField("receiver")
    private String receiver;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getPositionID() {
        return positionID;
    }

    public void setPositionID(Integer positionID) {
        this.positionID = positionID;
    }

    public Integer getHid() {
        return hid;
    }

    public void setHid(Integer hid) {
        this.hid = hid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPlocation() {
        return plocation;
    }

    public void setPlocation(String plocation) {
        this.plocation = plocation;
    }

    public String getPsalary() {
        return psalary;
    }

    public void setPsalary(String psalary) {
        this.psalary = psalary;
    }

    public String getPdescribe() {
        return pdescribe;
    }

    public void setPdescribe(String pdescribe) {
        this.pdescribe = pdescribe;
    }

    public Integer getPnumber() {
        return pnumber;
    }

    public void setPnumber(Integer pnumber) {
        this.pnumber = pnumber;
    }

    public Integer getPapplied() {
        return papplied;
    }

    public void setPapplied(Integer papplied) {
        this.papplied = papplied;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Positionneed() {
    }

    public Positionneed(Integer positionID, Integer hid, String pname, String plocation, String psalary, String pdescribe, Integer pnumber, Integer papplied, String receiver) {
        this.positionID = positionID;
        this.hid = hid;
        this.pname = pname;
        this.plocation = plocation;
        this.psalary = psalary;
        this.pdescribe = pdescribe;
        this.pnumber = pnumber;
        this.papplied = papplied;
        this.receiver = receiver;
    }
}
