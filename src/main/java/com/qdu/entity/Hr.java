package com.qdu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qdu.entity.sys.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
public class Hr extends BaseEntityNOSU implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "HID", type = IdType.AUTO)
      private Integer hid;
    @ApiModelProperty(value = "帐号")
    @TableField("Hname")
    private String Hname;
    @ApiModelProperty(value = "密码")
    @TableField("Hpwd")
    private String Hpwd;
    @ApiModelProperty(value = "昵称")
    @TableField("Hnickname")
    private String Hnickname;
    @ApiModelProperty(value = "图标")
    @TableField("Hicon")
    private String Hicon;
    @ApiModelProperty(value = "联系方式")
    @TableField("Hcontact")
    private String Hcontact;
    @ApiModelProperty(value = "邮箱")
    @TableField("Hemail")
    private String Hemail;
    @ApiModelProperty(value = "性别")
    @TableField("Hgender")
    private String Hgender;
    @ApiModelProperty(value = "职务")
    @TableField("Hduty")
    private String Hduty;
    @ApiModelProperty(value = "公司ID")
    @TableField("FID")
    private Integer fid;
    @ApiModelProperty(value = "是否锁定")
    private Boolean isLocked;
    @ApiModelProperty(value = "登录次数")
    private Long loginCount;
    @ApiModelProperty(value = "最后登录时间")
    private Date lastLoginTime;
    @ApiModelProperty(value = "角色集合,逗号分隔")
    @TableField(exist = false)
    private String roles;

}
