package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.vo.ApplyVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface ApplyVoMapper extends BaseMapper<ApplyVO> {

    @Select("select a.*,u.UName,u.Uicon,u.Ugender,u.Ubirth,u.Uedu,u.Uexp,u.Utel,u.Uemail,p.PositionID,p.Papplied from apply a,users u,positionneed p where a.UID=u.Uid and a.PosititionID=p.PositionID and p.PositionID=#{positionID} and a.Aisemploy=99")
    List<ApplyVO> showApplyList(Integer positionID);
}
