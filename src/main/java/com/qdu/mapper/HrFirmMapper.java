package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.Hr;
import com.qdu.vo.HrVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface HrFirmMapper extends BaseMapper<HrVO> {

    @Select("select h.*,f.Fname,f.Fename,f.Fdescribe,f.Ftype,f.Findustry,f.Flocation from hr h,firm f where h.FID=f.FID and h.FID=#{fid}")
    HrVO findOneDetail(Integer fid);


}
