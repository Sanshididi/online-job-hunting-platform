package com.qdu.mapper;

import com.qdu.entity.Message;
import java.util.List;

public interface MessageMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table message
     *
     * @mbg.generated Sun Dec 13 16:24:15 CST 2020
     */
    int deleteByPrimaryKey(Integer messageid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table message
     *
     * @mbg.generated Sun Dec 13 16:24:15 CST 2020
     */
    int insert(Message record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table message
     *
     * @mbg.generated Sun Dec 13 16:24:15 CST 2020
     */
    Message selectByPrimaryKey(Integer messageid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table message
     *
     * @mbg.generated Sun Dec 13 16:24:15 CST 2020
     */
    List<Message> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table message
     *
     * @mbg.generated Sun Dec 13 16:24:15 CST 2020
     */
    int updateByPrimaryKey(Message record);
}