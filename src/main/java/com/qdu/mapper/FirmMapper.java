package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.Firm;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Mapper
public interface FirmMapper extends BaseMapper<Firm> {

}
