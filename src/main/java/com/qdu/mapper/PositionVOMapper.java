package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.Apply;
import com.qdu.vo.PositionVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface PositionVOMapper extends BaseMapper<PositionVO> {

    @Select("select p.*,f.*,h.Hicon,h.Hnickname,h.Hgender from hr h,firm f,positionneed p where h.FID=f.FID and p.HID=h.HID and p.PositionID=#{positionID}")
    PositionVO showPositionHrFirmList(Integer positionID);
}
