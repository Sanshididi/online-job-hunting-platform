package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.Inform;
import com.qdu.entity.Positionneed;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
@Repository
public interface PositionneedMapper extends BaseMapper<Positionneed> {

    /**
     * <p>
     *   收藏职位
     * </p>
     * @author Zom
     * @since 2020-12-16
     */
    List<Positionneed> selectByReceiver(String receiver);

    void updateByPositionId(String positionId,String receiver);

    int deleteFavorite(String positionID);
}
