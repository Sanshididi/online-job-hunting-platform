package com.qdu.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.Apply;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ice
 * @since 2020-12-08
 */
public interface ApplyMapper extends BaseMapper<Apply> {

}
