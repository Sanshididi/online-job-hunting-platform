package com.qdu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qdu.entity.Hr;
import com.qdu.vo.HrVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface HrVoMapper extends BaseMapper<HrVO> {
    /**
     * @author:sanShi
     * @Description:{myBatis-plus wrapper自定义sql}
     * @date :2020/12/1
     */
    String querySql = "select h.* ,f.Fname FROM hr AS h LEFT JOIN firm AS f ON f.FID=h.FID WHERE h.is_deleted=0 ";
    String wrapperSql = "SELECT * from ( " + querySql + " ) AS q ${ew.customSqlSegment}";
    @Select(wrapperSql)
    IPage<HrVO> getHrALL(Page page, @Param(Constants.WRAPPER) Wrapper wrapper);
}
