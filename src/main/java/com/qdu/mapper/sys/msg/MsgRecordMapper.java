package com.qdu.mapper.sys.msg;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.sys.msg.MsgRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MsgRecordMapper extends BaseMapper<MsgRecord> {
}
