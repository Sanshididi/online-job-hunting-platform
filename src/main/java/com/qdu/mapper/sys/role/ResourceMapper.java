package com.qdu.mapper.sys.role;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.sys.role.Resource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {
}
