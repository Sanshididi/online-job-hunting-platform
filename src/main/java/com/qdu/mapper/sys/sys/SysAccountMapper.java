package com.qdu.mapper.sys.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.sys.sys.SysAccount;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Sanshi
 * @Description: {}
 * @date 2020/12/31
 */
@Mapper
public interface SysAccountMapper extends BaseMapper<SysAccount> {
}
