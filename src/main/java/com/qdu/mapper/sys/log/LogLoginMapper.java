package com.qdu.mapper.sys.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qdu.entity.sys.log.LogLogin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogLoginMapper extends BaseMapper<LogLogin> {
}
