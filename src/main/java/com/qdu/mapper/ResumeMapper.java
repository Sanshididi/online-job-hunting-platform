package com.qdu.mapper;

import com.qdu.entity.Resume;

import java.util.List;

public interface ResumeMapper {
    int deleteByPrimaryKey(Integer rid);

    int insert(Resume r);



    List<Resume> getAll(Integer uid);




    Resume getOneById(String rId);

    void updateByPrimaryKey(Resume r);
}