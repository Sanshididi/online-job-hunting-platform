package com.qdu.mapper;

import com.qdu.entity.Positionneed;
import com.qdu.entity.Resume;


import java.util.List;

/**
 * @author Lenovo
 */
public interface SearchMapper {
    List<Positionneed> getAll();
    Positionneed getOneById(String pId);
    List<Positionneed> getByBatchName(String pname);
}
