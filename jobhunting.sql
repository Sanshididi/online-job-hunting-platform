/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : jobhunting

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 29/12/2020 09:38:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `Id` int NOT NULL DEFAULT 1 COMMENT '主键',
  `Aname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'admin' COMMENT '用户名',
  `Apwd` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'admin' COMMENT '密码',
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `Id_UNIQUE`(`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci KEY_BLOCK_SIZE = 1 ROW_FORMAT = COMPRESSED;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'admin');

-- ----------------------------
-- Table structure for apply
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply`  (
  `AID` int NOT NULL AUTO_INCREMENT,
  `UID` int NOT NULL,
  `PosititionID` int NOT NULL,
  `Aisemploy` int NULL DEFAULT 99 COMMENT '0代表不录用，1代表录用，默认为99表示未处理',
  PRIMARY KEY (`AID`) USING BTREE,
  INDEX `FK_Apply_Pneed_PID`(`PosititionID`) USING BTREE,
  INDEX `FK_Apply_Users`(`UID`) USING BTREE,
  CONSTRAINT `FK_Apply_Positionneed` FOREIGN KEY (`PosititionID`) REFERENCES `positionneed` (`PositionID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Apply_Users` FOREIGN KEY (`UID`) REFERENCES `users` (`Uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of apply
-- ----------------------------
INSERT INTO `apply` VALUES (19, 3, 1, 99);
INSERT INTO `apply` VALUES (21, 1, 1, 0);
INSERT INTO `apply` VALUES (22, 2, 1, 0);

-- ----------------------------
-- Table structure for firm
-- ----------------------------
DROP TABLE IF EXISTS `firm`;
CREATE TABLE `firm`  (
  `FID` int NOT NULL AUTO_INCREMENT,
  `Fname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Fename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Fdescribe` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Ftype` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Findustry` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Flocation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `mender` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`FID`) USING BTREE,
  INDEX `FID`(`FID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of firm
-- ----------------------------
INSERT INTO `firm` VALUES (1, '说好不哭', 'dontcry', '一个神秘的组织', '私营企业', '互联网', '青岛', b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (2, '说好别哭', 'baobeibieku', '一个更神秘的组织', '私营企业', '互联网', '济宁', b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (3, '123', NULL, '123', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (4, '123', NULL, '123', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (5, '123', NULL, '123', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (6, '123', NULL, '123', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (7, '123', NULL, '123', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (8, 'sanshi', NULL, 'baba', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (9, '123', NULL, 'baba', NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);
INSERT INTO `firm` VALUES (10, 'bitDance', NULL, 'jay', NULL, NULL, NULL, b'0', '2020-12-20 12:39:27', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for hr
-- ----------------------------
DROP TABLE IF EXISTS `hr`;
CREATE TABLE `hr`  (
  `HID` int NOT NULL AUTO_INCREMENT,
  `Hname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Hpwd` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Hnickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Hicon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'lxh01.gif',
  `Hcontact` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Hemail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Hgender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Hduty` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FID` int NOT NULL,
  `is_locked` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否锁定（0:未锁定;1:已锁定）',
  `login_count` int NULL DEFAULT NULL COMMENT '登录次数',
  `note` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `creator` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `mender` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`HID`) USING BTREE,
  INDEX `FK_hr_firm_fid`(`FID`) USING BTREE,
  CONSTRAINT `FK_hr_firm_fid` FOREIGN KEY (`FID`) REFERENCES `firm` (`FID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hr
-- ----------------------------
INSERT INTO `hr` VALUES (1, 'ice', 'ice', 'Ice', 'lxh01.gif', '17866629660', '520@163.com', '男', 'HR', 1, b'0', NULL, NULL, b'0', NULL, NULL, '2020-12-27 14:56:04', NULL, NULL);
INSERT INTO `hr` VALUES (2, 'meng', 'meng', 'Mxz', 'lxh01.gif', '17866629660', '520@163.com', '男', 'HR', 2, b'0', NULL, NULL, b'0', NULL, NULL, '2020-12-18 20:33:34', NULL, NULL);
INSERT INTO `hr` VALUES (6, '123', '123', '123', 'lxh01.gif', '15106830253', NULL, NULL, 'HR', 9, b'1', NULL, NULL, b'0', NULL, NULL, '2020-12-18 20:33:42', NULL, NULL);
INSERT INTO `hr` VALUES (7, 'hr', 'e10adc3949ba59abbe56e057f20f883e', 'hr', 'lxh01.gif', '15621417563', NULL, NULL, 'HR', 10, b'1', 2, NULL, b'0', '2020-12-20 12:40:59', '2020-12-20 12:39:27', '2020-12-20 12:40:59', NULL, NULL);

-- ----------------------------
-- Table structure for inform
-- ----------------------------
DROP TABLE IF EXISTS `inform`;
CREATE TABLE `inform`  (
  `informid` int NOT NULL AUTO_INCREMENT,
  `receiver` int NOT NULL,
  `isread` int NOT NULL DEFAULT 0,
  `createtime` datetime(6) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `positionId` int NOT NULL,
  PRIMARY KEY (`informid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inform
-- ----------------------------
INSERT INTO `inform` VALUES (1, 7, 0, '2020-12-21 16:11:13.419000', '求职小助手通知', '您向后端开发工程师投递的简历，状态已更新，请及时查看。', 1);
INSERT INTO `inform` VALUES (2, 0, 0, '2020-12-28 22:45:04.750000', '求职小助手通知', '您向JAVA开发实习生2投递的简历，状态已更新，请及时查看。', 4);
INSERT INTO `inform` VALUES (3, 7, 0, '2020-12-28 23:03:58.797000', '求职小助手通知', '您向JAVA开发实习生2投递的简历，状态已更新，请及时查看。', 4);
INSERT INTO `inform` VALUES (4, 7, 0, '2020-12-29 09:05:24.445000', '求职小助手通知', '您向跨境电商总经理投递的简历，状态已更新，请及时查看。', 7);
INSERT INTO `inform` VALUES (5, 8, 0, '2020-12-29 09:05:43.502000', '求职小助手通知', '您向高级运维工程师投递的简历，状态已更新，请及时查看。', 9);
INSERT INTO `inform` VALUES (6, 7, 0, '2020-12-29 09:07:58.476000', '求职小助手通知', '您向HTML5讲师投递的简历，状态已更新，请及时查看。', 5);

-- ----------------------------
-- Table structure for log_login
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `account_id` bigint NOT NULL COMMENT '用户id',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '帐号',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ip地址',
  `category_id` bigint NULL DEFAULT NULL COMMENT '分类id',
  `category_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类name',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 196 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of log_login
-- ----------------------------
INSERT INTO `log_login` VALUES (1, 1, '18512345671', 'Sasnhi', '58.246.141.21', 1, '密码登录', b'0', '2019-09-08 19:20:39');
INSERT INTO `log_login` VALUES (2, 1, '18512345671', 'Sasnhi', '127.0.0.1', 1, '密码登录', b'0', '2019-09-08 19:50:23');
INSERT INTO `log_login` VALUES (3, 1, '18512345671', 'Sasnhi', '58.246.143.53', 1, '密码登录', b'0', '2019-09-09 10:13:18');
INSERT INTO `log_login` VALUES (165, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', NULL);
INSERT INTO `log_login` VALUES (166, 5, 'sa', 'sa', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', NULL);
INSERT INTO `log_login` VALUES (167, 5, 'sa', 'sa', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', NULL);
INSERT INTO `log_login` VALUES (168, 5, 'sa', 'sa', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-14 22:28:09');
INSERT INTO `log_login` VALUES (169, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-14 23:00:19');
INSERT INTO `log_login` VALUES (170, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-15 11:00:27');
INSERT INTO `log_login` VALUES (171, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-15 11:37:27');
INSERT INTO `log_login` VALUES (172, 7, '15106830253', '15106830253', '0:0:0:0:0:0:0:1', 0, '注册登录', b'0', '2020-12-16 09:26:13');
INSERT INTO `log_login` VALUES (173, 7, '15106830253', '15106830253', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-16 18:51:25');
INSERT INTO `log_login` VALUES (174, 7, '15106830253', '15106830253', '127.0.0.1', 1, '密码登录', b'0', '2020-12-16 18:52:24');
INSERT INTO `log_login` VALUES (175, 7, '15106830253', '15106830253', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-16 18:53:08');
INSERT INTO `log_login` VALUES (176, 7, '15106830253', '15106830253', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-16 18:54:44');
INSERT INTO `log_login` VALUES (177, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-16 18:57:24');
INSERT INTO `log_login` VALUES (178, 7, '15106830253', '15106830253', '0:0:0:0:0:0:0:1', 2, '验证码登录', b'0', '2020-12-16 19:05:00');
INSERT INTO `log_login` VALUES (179, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-16 19:05:24');
INSERT INTO `log_login` VALUES (180, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-17 01:52:36');
INSERT INTO `log_login` VALUES (181, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-17 11:21:54');
INSERT INTO `log_login` VALUES (182, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-17 16:31:18');
INSERT INTO `log_login` VALUES (183, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-17 17:07:22');
INSERT INTO `log_login` VALUES (184, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-17 18:26:21');
INSERT INTO `log_login` VALUES (185, 1, '18512345671', 'Sasnhi', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-18 00:26:27');
INSERT INTO `log_login` VALUES (186, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-18 18:09:33');
INSERT INTO `log_login` VALUES (187, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-18 21:05:04');
INSERT INTO `log_login` VALUES (188, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-18 23:59:12');
INSERT INTO `log_login` VALUES (189, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-19 00:23:27');
INSERT INTO `log_login` VALUES (190, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-19 10:04:53');
INSERT INTO `log_login` VALUES (191, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-19 10:26:02');
INSERT INTO `log_login` VALUES (192, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-19 11:00:55');
INSERT INTO `log_login` VALUES (193, 4, 'admin', 'admin', '0:0:0:0:0:0:0:1', 1, '密码登录', b'0', '2020-12-19 14:17:40');
INSERT INTO `log_login` VALUES (194, 4, 'admin', 'admin', '192.168.159.1', 1, '密码登录', b'0', '2020-12-20 11:11:49');
INSERT INTO `log_login` VALUES (195, 4, 'admin', 'admin', '192.168.159.1', 1, '密码登录', b'0', '2020-12-20 12:42:17');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `messageid` int NOT NULL,
  `sender` int NOT NULL,
  `receiver` int NOT NULL,
  `sendername` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sendericon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `note` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `isread` int NOT NULL,
  `createtime` datetime(6) NOT NULL,
  PRIMARY KEY (`messageid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for msg_record
-- ----------------------------
DROP TABLE IF EXISTS `msg_record`;
CREATE TABLE `msg_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '标识-主键',
  `category` bigint NOT NULL COMMENT '类型（1.注册，2.找回密码等）',
  `category_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信内容',
  `sent_time` datetime(0) NULL DEFAULT NULL COMMENT '发送时间',
  `result` bit(1) NULL DEFAULT NULL COMMENT '结果',
  `result_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '验证码记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of msg_record
-- ----------------------------
INSERT INTO `msg_record` VALUES (1, 2, '登录验证码', '18512345672', '290576', '2019-09-10 11:45:05', b'1', '{\"Message\":\"OK\",\"RequestId\":\"CBD238AC-4B69-4951-9368-30694691295E\",\"BizId\":\"927002968087104732^0\",\"Code\":\"OK\"}', '2019-09-10 11:45:05');
INSERT INTO `msg_record` VALUES (2, 2, '登录验证码', '18512345672', '848828', '2019-09-10 11:57:31', b'1', '{\"Message\":\"OK\",\"RequestId\":\"FA69C900-464F-456B-AF3C-05146AE49BAB\",\"BizId\":\"816712468087851087^0\",\"Code\":\"OK\"}', '2019-09-10 11:57:31');
INSERT INTO `msg_record` VALUES (3, 1, '注册验证码', '18512345672', '150702', '2019-09-10 11:58:03', b'1', '{\"Message\":\"OK\",\"RequestId\":\"56CA20EC-DC3E-47D4-A114-CE758D551629\",\"BizId\":\"598413268087882704^0\",\"Code\":\"OK\"}', '2019-09-10 11:58:03');
INSERT INTO `msg_record` VALUES (4, 1, '注册验证码', '18512345671', '990938', '2020-11-28 16:10:46', b'1', '{\"Message\":\"OK\",\"RequestId\":\"482A41BD-004F-4DF1-B46A-B5DD9BDCD0EB\",\"Code\":\"OK\",\"BizId\":\"701319906551045361^0\"}', '2020-11-28 16:10:46');
INSERT INTO `msg_record` VALUES (5, 1, '注册验证码', '15621417563', '903860', '2020-11-28 16:11:09', b'1', '{\"Message\":\"OK\",\"RequestId\":\"51867B07-4AA3-492C-82C7-AF91CA47FF93\",\"Code\":\"OK\",\"BizId\":\"682811406551069004^0\"}', '2020-11-28 16:11:09');
INSERT INTO `msg_record` VALUES (6, 1, '注册验证码', '15621417563', '525854', '2020-12-15 21:36:16', b'0', '{\"Message\":\"签名不合法(不存在或被拉黑)\",\"RequestId\":\"5552BF66-8FA1-437D-A880-684223932A81\",\"Code\":\"isv.SMS_SIGNATURE_ILLEGAL\"}', '2020-12-15 21:36:16');
INSERT INTO `msg_record` VALUES (7, 1, '注册验证码', '15106830253', '385794', '2020-12-15 21:36:49', b'0', '{\"Message\":\"签名不合法(不存在或被拉黑)\",\"RequestId\":\"E63BEF77-962B-4FF9-9AC6-EA22D04D3103\",\"Code\":\"isv.SMS_SIGNATURE_ILLEGAL\"}', '2020-12-15 21:36:49');
INSERT INTO `msg_record` VALUES (8, 1, '注册验证码', '15621417563', '502907', '2020-12-15 21:48:52', b'0', '{\"Message\":\"签名不合法(不存在或被拉黑)\",\"RequestId\":\"C87B40BA-4E6C-42A1-B8AB-EE655AED45A7\",\"Code\":\"isv.SMS_SIGNATURE_ILLEGAL\"}', '2020-12-15 21:48:52');
INSERT INTO `msg_record` VALUES (9, 1, '注册验证码', '15621417563', '692968', '2020-12-15 21:50:41', b'0', '{\"Message\":\"签名不合法(不存在或被拉黑)\",\"RequestId\":\"4F98EF57-2161-4A5F-8A33-B496886A8409\",\"Code\":\"isv.SMS_SIGNATURE_ILLEGAL\"}', '2020-12-15 21:50:41');
INSERT INTO `msg_record` VALUES (10, 1, '注册验证码', '15621417563', '926473', '2020-12-15 21:53:53', b'0', '{\"Message\":\"签名不合法(不存在或被拉黑)\",\"RequestId\":\"DC904E60-1260-40FB-AFE0-FD12ED6611FA\",\"Code\":\"isv.SMS_SIGNATURE_ILLEGAL\"}', '2020-12-15 21:53:53');
INSERT INTO `msg_record` VALUES (11, 1, '注册验证码', '15621417563', '914522', '2020-12-15 23:21:48', b'1', '{\"Message\":\"OK\",\"RequestId\":\"676E89A7-C78B-41CE-9DB2-7A678E881801\",\"Code\":\"OK\",\"BizId\":\"546412208045708410^0\"}', '2020-12-15 23:21:48');
INSERT INTO `msg_record` VALUES (12, 1, '注册验证码', '17866635047', '634689', '2020-12-15 23:22:33', b'1', '{\"Message\":\"OK\",\"RequestId\":\"ACA96F82-AD5E-4267-8B5A-2B6F2C672E11\",\"Code\":\"OK\",\"BizId\":\"288421308045753519^0\"}', '2020-12-15 23:22:33');
INSERT INTO `msg_record` VALUES (13, 1, '注册验证码', '15621417654', '475267', '2020-12-16 08:40:48', b'1', '{\"Message\":\"OK\",\"RequestId\":\"D04A87E3-D1E2-4F7A-9E67-C356AD99A92D\",\"Code\":\"OK\",\"BizId\":\"637823908079247355^0\"}', '2020-12-16 08:40:48');
INSERT INTO `msg_record` VALUES (14, 1, '注册验证码', '15106830253', '691471', '2020-12-16 09:25:53', b'1', '{\"Message\":\"OK\",\"RequestId\":\"6E1E6099-0886-4943-A5A0-DB54AEF1EC70\",\"Code\":\"OK\",\"BizId\":\"853721908081952522^0\"}', '2020-12-16 09:25:53');
INSERT INTO `msg_record` VALUES (15, 2, '登录验证码', '15106830253', '404541', '2020-12-16 19:01:13', b'0', '{\"Message\":\"模板不合法(不存在或被拉黑)\",\"RequestId\":\"645036F9-0BC9-4FA9-82F2-A6B2EF749DE4\",\"Code\":\"isv.SMS_TEMPLATE_ILLEGAL\"}', '2020-12-16 19:01:13');
INSERT INTO `msg_record` VALUES (16, 2, '登录验证码', '15106830253', '496497', '2020-12-16 19:04:44', b'1', '{\"Message\":\"OK\",\"RequestId\":\"5658ED2D-E645-434F-A09A-40EA3C9BBA29\",\"Code\":\"OK\",\"BizId\":\"357607308116684068^0\"}', '2020-12-16 19:04:44');
INSERT INTO `msg_record` VALUES (17, 1, '注册验证码', '15106830253', '135908', '2020-12-16 23:06:30', b'1', '{\"Message\":\"OK\",\"RequestId\":\"BC606E45-58EB-4269-AB05-0368AE32B80F\",\"Code\":\"OK\",\"BizId\":\"102905008131189302^0\"}', '2020-12-16 23:06:30');
INSERT INTO `msg_record` VALUES (18, 1, '注册验证码', '15106830253', '658969', '2020-12-17 00:36:06', b'1', '{\"Message\":\"OK\",\"RequestId\":\"05892233-E545-4CCB-8047-56EEB9DC5D35\",\"Code\":\"OK\",\"BizId\":\"850301408136565359^0\"}', '2020-12-17 00:36:06');
INSERT INTO `msg_record` VALUES (19, 1, '注册验证码', '15106830253', '981051', '2020-12-17 00:38:45', b'1', '{\"Message\":\"OK\",\"RequestId\":\"D872A5B8-2180-4DE1-B10F-778ACB91C396\",\"Code\":\"OK\",\"BizId\":\"678118008136724629^0\"}', '2020-12-17 00:38:45');
INSERT INTO `msg_record` VALUES (20, 1, '注册验证码', '15106830253', '538704', '2020-12-17 00:58:19', b'1', '{\"Message\":\"OK\",\"RequestId\":\"4D9A7C57-6FB1-4DC2-BE10-C59DDDF8FBE5\",\"Code\":\"OK\",\"BizId\":\"439723908137899049^0\"}', '2020-12-17 00:58:19');
INSERT INTO `msg_record` VALUES (21, 1, '注册验证码', '15621417563', '119962', '2020-12-17 01:00:07', b'1', '{\"Message\":\"OK\",\"RequestId\":\"F6075680-1742-4280-AE38-1E197B1CE8B4\",\"Code\":\"OK\",\"BizId\":\"330305608138006712^0\"}', '2020-12-17 01:00:07');
INSERT INTO `msg_record` VALUES (22, 1, '注册验证码', '15106830253', '469551', '2020-12-17 01:06:28', b'1', '{\"Message\":\"OK\",\"RequestId\":\"A55AAA31-9178-4C66-9C72-CD4CCD5B9880\",\"Code\":\"OK\",\"BizId\":\"469210608138387922^0\"}', '2020-12-17 01:06:28');
INSERT INTO `msg_record` VALUES (23, 1, '注册验证码', '15106830253', '708844', '2020-12-17 01:08:47', b'1', '{\"Message\":\"OK\",\"RequestId\":\"55CF0C34-DD4D-41C9-8C71-AB9D0A83D3A0\",\"Code\":\"OK\",\"BizId\":\"442509008138527119^0\"}', '2020-12-17 01:08:47');
INSERT INTO `msg_record` VALUES (24, 1, '注册验证码', '15106830253', '970296', '2020-12-17 01:17:00', b'1', '{\"Message\":\"OK\",\"RequestId\":\"612CED6B-8392-418B-A5B7-8C67DFBA779C\",\"Code\":\"OK\",\"BizId\":\"837124908139019398^0\"}', '2020-12-17 01:17:00');
INSERT INTO `msg_record` VALUES (25, 1, '注册验证码', '15106830253', '165461', '2020-12-20 10:48:42', b'1', '{\"Message\":\"OK\",\"RequestId\":\"968EF969-F580-4516-9086-A1E7ED9CB09D\",\"Code\":\"OK\",\"BizId\":\"919107308432521690^0\"}', '2020-12-20 10:48:42');
INSERT INTO `msg_record` VALUES (26, 2, '登录验证码', '15106830253', '780936', '2020-12-20 10:58:33', b'1', '{\"Message\":\"OK\",\"RequestId\":\"5B3DF7C9-0FC2-4FB2-9AC2-18022698F8C6\",\"Code\":\"OK\",\"BizId\":\"764820108433113094^0\"}', '2020-12-20 10:58:33');
INSERT INTO `msg_record` VALUES (27, 2, '登录验证码', '15160932452', '437963', '2020-12-20 11:00:47', b'1', '{\"Message\":\"OK\",\"RequestId\":\"75B2B192-0407-44DF-B88D-306C898FCB35\",\"Code\":\"OK\",\"BizId\":\"179720708433246461^0\"}', '2020-12-20 11:00:47');
INSERT INTO `msg_record` VALUES (28, 1, '注册验证码', '15621417563', '481266', '2020-12-20 11:02:07', b'1', '{\"Message\":\"OK\",\"RequestId\":\"1EF20AF8-82A3-46BF-9E7F-684967D87492\",\"Code\":\"OK\",\"BizId\":\"930001508433327031^0\"}', '2020-12-20 11:02:07');
INSERT INTO `msg_record` VALUES (29, 1, '注册验证码', '15621417563', '409540', '2020-12-20 12:39:04', b'1', '{\"Message\":\"OK\",\"RequestId\":\"F9C04821-5F9B-4F55-9087-A9BD40474B0F\",\"Code\":\"OK\",\"BizId\":\"167318308439144002^0\"}', '2020-12-20 12:39:04');
INSERT INTO `msg_record` VALUES (30, 1, '注册验证码', '17866635047', '804786', '2020-12-28 11:24:19', b'1', '{\"Message\":\"OK\",\"RequestId\":\"28DB8E4D-61F1-4768-907A-AC4354BD0571\",\"Code\":\"OK\",\"BizId\":\"980624009125809859^0\"}', '2020-12-28 11:24:19');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint NOT NULL COMMENT '角色标识',
  `submenu_id` bigint NULL DEFAULT NULL COMMENT '菜单标识',
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `mender` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 512 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (65, 4, 7, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (66, 4, 8, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (67, 4, 9, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (68, 4, 24, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (69, 4, 25, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (70, 4, 26, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (71, 4, 27, NULL, b'1', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `permission` VALUES (111, 6, 1, NULL, b'1', '18512345671', '2019-09-01 13:16:02', NULL, NULL);
INSERT INTO `permission` VALUES (112, 6, 2, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (113, 6, 3, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (114, 6, 4, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (115, 6, 5, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (116, 6, 6, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (117, 6, 12, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (118, 6, 13, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (119, 6, 14, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (120, 6, 15, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (121, 6, 16, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (122, 6, 17, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (123, 6, 18, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (124, 6, 19, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (125, 6, 20, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (126, 6, 21, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (127, 6, 22, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (128, 6, 23, NULL, b'1', '18512345671', '2019-09-01 13:16:03', NULL, NULL);
INSERT INTO `permission` VALUES (249, 1, 1, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (250, 1, 2, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (251, 1, 3, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (252, 1, 4, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (253, 1, 5, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (254, 1, 6, NULL, b'1', '18512345671', '2019-09-05 16:49:32', NULL, NULL);
INSERT INTO `permission` VALUES (255, 1, 12, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (256, 1, 13, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (257, 1, 14, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (258, 1, 15, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (259, 1, 16, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (260, 1, 17, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (261, 1, 18, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (262, 1, 19, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (263, 1, 20, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (264, 1, 21, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (265, 1, 22, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (266, 1, 23, NULL, b'1', '18512345671', '2019-09-05 16:49:33', NULL, NULL);
INSERT INTO `permission` VALUES (267, 1, 7, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (268, 1, 8, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (269, 1, 9, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (270, 1, 24, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (271, 1, 25, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (272, 1, 26, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (273, 1, 27, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (274, 1, 10, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (275, 1, 11, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (276, 1, 28, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (277, 1, 29, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (278, 1, 30, NULL, b'1', '18512345671', '2019-09-05 16:49:34', NULL, NULL);
INSERT INTO `permission` VALUES (279, 1, 31, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (280, 1, 32, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (281, 1, 33, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (282, 1, 34, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (283, 1, 35, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (284, 1, 36, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (285, 1, 39, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (286, 1, 37, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (287, 1, 38, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (288, 1, 40, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (289, 1, 41, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (290, 1, 42, NULL, b'1', '18512345671', '2019-09-05 16:49:35', NULL, NULL);
INSERT INTO `permission` VALUES (301, 5, 10, NULL, b'1', '18512345671', '2019-09-08 14:16:20', NULL, NULL);
INSERT INTO `permission` VALUES (302, 5, 11, NULL, b'1', '18512345671', '2019-09-08 14:16:20', NULL, NULL);
INSERT INTO `permission` VALUES (303, 5, 28, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (304, 5, 29, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (305, 5, 30, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (306, 5, 31, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (307, 5, 32, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (308, 5, 33, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (309, 5, 34, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (310, 5, 35, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (311, 5, 36, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (312, 5, 39, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (313, 5, 37, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (314, 5, 38, NULL, b'1', '18512345671', '2019-09-08 14:16:21', NULL, NULL);
INSERT INTO `permission` VALUES (325, 2, 33, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (326, 2, 34, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (327, 2, 35, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (328, 2, 36, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (329, 2, 39, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (330, 2, 37, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (331, 2, 38, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (332, 2, 40, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (333, 2, 41, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (334, 2, 42, NULL, b'0', '18512345671', '2019-09-08 14:16:46', NULL, NULL);
INSERT INTO `permission` VALUES (345, 7, 1, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (346, 7, 2, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (347, 7, 3, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (348, 7, 4, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (349, 7, 5, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (350, 7, 6, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (351, 7, 12, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (352, 7, 13, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (353, 7, 14, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (354, 7, 15, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (355, 7, 16, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (356, 7, 17, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (357, 7, 18, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (358, 7, 19, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (359, 7, 20, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (360, 7, 21, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (361, 7, 22, NULL, b'1', '18512345671', '2019-09-08 17:28:31', NULL, NULL);
INSERT INTO `permission` VALUES (362, 7, 23, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (363, 7, 7, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (364, 7, 8, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (365, 7, 9, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (366, 7, 24, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (367, 7, 25, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (368, 7, 26, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (369, 7, 27, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (370, 7, 10, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (371, 7, 11, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (372, 7, 28, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (373, 7, 29, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (374, 7, 30, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (375, 7, 31, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (376, 7, 32, NULL, b'1', '18512345671', '2019-09-08 17:28:32', NULL, NULL);
INSERT INTO `permission` VALUES (377, 7, 1, NULL, b'0', '18512345671', '2019-09-09 18:26:51', NULL, NULL);
INSERT INTO `permission` VALUES (378, 7, 2, NULL, b'0', '18512345671', '2019-09-09 18:26:51', NULL, NULL);
INSERT INTO `permission` VALUES (379, 7, 3, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (380, 7, 4, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (381, 7, 5, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (382, 7, 6, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (383, 7, 12, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (384, 7, 13, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (385, 7, 14, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (386, 7, 15, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (387, 7, 16, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (388, 7, 17, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (389, 7, 18, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (390, 7, 19, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (391, 7, 20, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (392, 7, 21, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (393, 7, 22, NULL, b'0', '18512345671', '2019-09-09 18:26:52', NULL, NULL);
INSERT INTO `permission` VALUES (394, 7, 23, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (395, 7, 7, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (396, 7, 8, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (397, 7, 9, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (398, 7, 24, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (399, 7, 25, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (400, 7, 26, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (401, 7, 27, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (402, 7, 10, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (403, 7, 11, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (404, 7, 28, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (405, 7, 29, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (406, 7, 30, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (407, 7, 31, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (408, 7, 32, NULL, b'0', '18512345671', '2019-09-09 18:26:53', NULL, NULL);
INSERT INTO `permission` VALUES (409, 7, 33, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (410, 7, 34, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (411, 7, 35, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (412, 7, 36, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (413, 7, 39, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (414, 7, 37, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (415, 7, 38, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (416, 7, 40, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (417, 7, 41, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (418, 7, 42, NULL, b'0', '18512345671', '2019-09-09 18:26:54', NULL, NULL);
INSERT INTO `permission` VALUES (419, 1, 1, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (420, 1, 2, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (421, 1, 3, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (422, 1, 4, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (423, 1, 5, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (424, 1, 6, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (425, 1, 12, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (426, 1, 13, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (427, 1, 14, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (428, 1, 15, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (429, 1, 16, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (430, 1, 17, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (431, 1, 18, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (432, 1, 19, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (433, 1, 20, NULL, b'1', '18512345671', '2019-09-12 12:22:07', NULL, NULL);
INSERT INTO `permission` VALUES (434, 1, 21, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (435, 1, 22, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (436, 1, 23, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (437, 1, 7, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (438, 1, 8, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (439, 1, 9, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (440, 1, 24, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (441, 1, 25, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (442, 1, 26, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (443, 1, 27, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (444, 1, 10, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (445, 1, 11, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (446, 1, 28, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (447, 1, 29, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (448, 1, 30, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (449, 1, 31, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (450, 1, 32, NULL, b'1', '18512345671', '2019-09-12 12:22:08', NULL, NULL);
INSERT INTO `permission` VALUES (451, 1, 33, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (452, 1, 34, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (453, 1, 35, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (454, 1, 36, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (455, 1, 39, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (456, 1, 37, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (457, 1, 38, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (458, 1, 40, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (459, 1, 41, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (460, 1, 42, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (461, 1, 43, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (462, 1, 44, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (463, 1, 45, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (464, 1, 46, NULL, b'1', '18512345671', '2019-09-12 12:22:09', NULL, NULL);
INSERT INTO `permission` VALUES (465, 1, 1, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (466, 1, 2, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (467, 1, 3, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (468, 1, 4, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (469, 1, 5, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (470, 1, 6, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (471, 1, 12, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (472, 1, 13, NULL, b'0', '18512345671', '2019-09-12 12:44:49', NULL, NULL);
INSERT INTO `permission` VALUES (473, 1, 14, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (474, 1, 15, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (475, 1, 16, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (476, 1, 17, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (477, 1, 18, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (478, 1, 19, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (479, 1, 20, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (480, 1, 21, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (481, 1, 22, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (482, 1, 23, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (483, 1, 7, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (484, 1, 8, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (485, 1, 9, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (486, 1, 24, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (487, 1, 25, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (488, 1, 26, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (489, 1, 27, NULL, b'0', '18512345671', '2019-09-12 12:44:50', NULL, NULL);
INSERT INTO `permission` VALUES (490, 1, 10, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (491, 1, 11, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (492, 1, 28, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (493, 1, 29, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (494, 1, 30, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (495, 1, 31, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (496, 1, 32, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (497, 1, 33, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (498, 1, 34, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (499, 1, 35, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (500, 1, 36, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (501, 1, 39, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (502, 1, 37, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (503, 1, 38, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (504, 1, 40, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (505, 1, 41, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (506, 1, 42, NULL, b'0', '18512345671', '2019-09-12 12:44:51', NULL, NULL);
INSERT INTO `permission` VALUES (507, 1, 43, NULL, b'0', '18512345671', '2019-09-12 12:44:52', NULL, NULL);
INSERT INTO `permission` VALUES (508, 1, 44, NULL, b'0', '18512345671', '2019-09-12 12:44:52', NULL, NULL);
INSERT INTO `permission` VALUES (509, 1, 45, NULL, b'0', '18512345671', '2019-09-12 12:44:52', NULL, NULL);
INSERT INTO `permission` VALUES (510, 1, 46, NULL, b'0', '18512345671', '2019-09-12 12:44:52', NULL, NULL);
INSERT INTO `permission` VALUES (511, 1, 47, NULL, b'0', '18512345671', '2019-09-12 12:44:52', NULL, NULL);

-- ----------------------------
-- Table structure for positionneed
-- ----------------------------
DROP TABLE IF EXISTS `positionneed`;
CREATE TABLE `positionneed`  (
  `PositionID` int NOT NULL AUTO_INCREMENT,
  `HID` int NOT NULL,
  `Pname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Plocation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Psalary` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Pdescribe` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Pnumber` int NULL DEFAULT NULL,
  `Papplied` int NULL DEFAULT 0,
  `receiver` int NOT NULL,
  PRIMARY KEY (`PositionID`) USING BTREE,
  INDEX `FK_HR_PNeed_HID`(`HID`) USING BTREE,
  CONSTRAINT `FK_HR_PNeed_HID` FOREIGN KEY (`HID`) REFERENCES `hr` (`HID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of positionneed
-- ----------------------------
INSERT INTO `positionneed` VALUES (1, 1, '后端开发工程师', '北京', '25000元', '职位描述：\r\n1、负责架构设计和开发，为亿级用户提供优质顺畅的信息服务和极致体验；\r\n2、参与设计系统技术方案，核心代码开发和系统调优；\r\n3、参与制定代码规范、测试规范，建立起开发质量控制方法；\r\n4、协助团队攻克各种高并发、数据隔离、系统解耦等方面的技术难关；\r\n5、参与各专项技术调研，新技术引入等前瞻项目。\r\n职位要求：\r\n1、本科及以上学历，计算机相关专业；\r\n2、热爱计算机科学和互联网技术，精通至少一门编程语言，包括但不仅限于：Java、C、C++、PHP、 Python、Go；\r\n3、掌握扎实的计算机基础知识，深入理解数据结构、算法和操作系统知识；\r\n4、有优秀的逻辑分析能力，能够对业务逻辑进行合理的抽象和拆分；\r\n5、有强烈的求知欲，优秀的学习和沟通能力。 ', 20, 3, 7);
INSERT INTO `positionneed` VALUES (2, 1, '嵌入式软件工程师', '广东深圳', '15-30K·14薪', '对标互联网一线大厂的薪资待遇、965~1075弹性打卡不加班、每日60元餐补和免费下午茶、足额的社保与公积金（8%），可期的职业上升路线\r\n工作内容：\r\n1.负责Linux系统层驱动开发，应用功能开发；\r\n2.负责所属模块的链路、架构设计及开发编码等工作；\r\n3.进行软件代码维护，优化解决系统级问题。\r\n职位要求：\r\n1.本科及以上学历，3年以上工作经验；\r\n2.精通C/C++语言及调试技巧；\r\n3.熟悉Linux驱动开发及应用程序开发；\r\n4.熟悉数据结构及算法优先考虑。', 5, 2, 7);
INSERT INTO `positionneed` VALUES (4, 2, 'JAVA开发实习生2', '北京', '23-233元/天', '敢来就敢收，来就行', 1, 0, 7);
INSERT INTO `positionneed` VALUES (5, 1, 'HTML5讲师', '山东青岛', '15-25K', '岗位职责：\r\n（1）负责讲授JS相关课程；\r\n（2）协助主管完成各类教学资料的编写；\r\n（3）完成其他交办的工作。\r\n任职要求：\r\n（1）三年及以上工作经验，熟练运用HTML、CSS进行网页制作，熟悉CSS3新特性\r\n（2）熟练掌握原生JS及面向对象编程方法\r\n（3）掌握JS\r\n（4）熟练使用ajax\r\n（5）熟悉常见的JS设计\r\n（6）熟悉jquery、bootstrap\r\n（7）掌握sass或less预编译处\r\n（8）掌握常见前端开发工具gulp或web\r\n（9）掌握模块化开发requireJS或seaJS\r\n（10）了解ES6新规范\r\n3、优先条件\r\n（1）有nodeJS后端开发经验者优先\r\n（2）有php或jsp开发经验者优先\r\n（3）有移动端项目开发经验者优先', 5, 4, 7);
INSERT INTO `positionneed` VALUES (6, 1, '软件项目经理', '山东蓝翔', '25-30K', '1、负责需求评审、软件开发计划的制定、版本发布、软件成熟度评估等工作；\r\n2、按照软件开发计划，推进各模块的开发工作，保证软件工作按计划开展，并对最终的软件交付负责；\r\n3、负责项目的风险和问题识别，跟踪，闭环；\r\n4、负责与各相关部门沟通协调工作，负责与方案商的沟通协调工作；\r\n5、负责工厂生产问题、市场反馈问题的处理。', 15, 6, 8);
INSERT INTO `positionneed` VALUES (7, 1, '跨境电商总经理', '山东潍坊', '20-35k', '技能要求：\r\n跨境电商贸易，互联网\r\n岗位职责：\r\n1.跨境电商平台的整体运营规划\r\n2.产品上架及运营\r\n3.团队管理\r\n任职要求：\r\n1.有跨境电商平台运营经验，食品产业相关平台运营经验者优先\r\n2.本科及以上学历', 21, 3, 7);
INSERT INTO `positionneed` VALUES (8, 1, '产品项目经理', '山东济宁', '15-22K', '工作职责：\r\n1、 负责软件开发项目的管理工作，制定项目计划和控制项目进度；\r\n2、 承担项目的业务梳理及相关设计工作，参与核心代码编写，懂UML语言\r\n3、 指导工程师的开发工作，发现并指出开发过程中存在的问题并提出解决方案；\r\n4、 及时有效地解决设计、开发人员所提出的问题，解决技术上的难题；\r\n5、 参与制定并规范项目的系统开发文档、工具、模型;\r\n6、 参与软件需求与系统设计评审和代码检查、项目验收等工作;\r\n7、 领导交代的其他任务;\r\n8、能搭建并组建20人以上研发团队具有管理咨询经验及外包开发合作伙伴经验，有大型千万级软件开发管理经验优先。\r\n9、10年IT相关技术开发从业经验\r\n10、年龄37岁-55岁。。', 1, 0, 8);
INSERT INTO `positionneed` VALUES (9, 1, '高级运维工程师', '浙江杭州', '16-20K', '工作内容：\r\n1、负责IT项目运维；\r\n2、严格按照制定的流程及规范实行运维操作；\r\n3、负责系统部署上线、系统优化；\r\n4、负责运维报告的整理以及相关文档的编写；\r\n职位要求：\r\n1、3年以上linux系统运维工作经验，熟悉并使用过阿里云产品者优先；\r\n2、精通docker容器技术、熟悉k8s（编排技术，to G，有相关经验），熟悉ceph、kafka、spark等组件的运维。\r\n3、熟悉主流数据库，善于分析解决问题。\r\n4、具有良好的产品学习能力；\r\n5、具有独立分析解决问题的能力、团队合作意识及良好的文档记录习惯', 15, 5, 8);

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `category` bigint NOT NULL COMMENT '类别(1:目录,2:菜单,3:按钮)',
  `super_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父级id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问路径',
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '_self' COMMENT '打开方式',
  `ordinal` bigint NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单资源' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resource
-- ----------------------------
INSERT INTO `resource` VALUES (1, 1, '0', '常规管理', 'fa fa-address-book', NULL, NULL, 10);
INSERT INTO `resource` VALUES (2, 2, '1', '主页模板', 'fa fa-home', NULL, '_self', 1);
INSERT INTO `resource` VALUES (3, 2, '2', '主页一', 'fa fa-tachometer', 'page/welcome-1', '_self', 1);
INSERT INTO `resource` VALUES (4, 2, '2', '主页二', 'fa fa-tachometer', 'page/welcome-2', '_self', 2);
INSERT INTO `resource` VALUES (5, 2, '1', '菜单管理', 'fa fa-window-maximize', 'page/menu', '_self', 2);
INSERT INTO `resource` VALUES (6, 2, '1', '系统设置', 'fa fa-gears', 'page/setting', '_self', 3);
INSERT INTO `resource` VALUES (7, 1, '0', '组件管理', 'fa fa-lemon-o', NULL, NULL, 20);
INSERT INTO `resource` VALUES (8, 2, '7', '图标列表', 'fa fa-dot-circle-o', 'page/icon', '_self', 1);
INSERT INTO `resource` VALUES (9, 2, '7', '图标选择', 'fa fa-adn', 'page/icon-picker', '_self', 2);
INSERT INTO `resource` VALUES (10, 1, '0', '其它管理', 'fa fa-slideshare', NULL, NULL, 30);
INSERT INTO `resource` VALUES (11, 2, '10', '失效菜单', 'fa fa-superpowers', 'page/error', '_self', 1);
INSERT INTO `resource` VALUES (12, 2, '1', '表格示例', 'fa fa-file-text', 'page/table', '_self', 4);
INSERT INTO `resource` VALUES (13, 2, '1', '表单示例', 'fa fa-calendar', NULL, NULL, 5);
INSERT INTO `resource` VALUES (14, 2, '13', '普通表单', 'fa fa-list-alt', 'page/form', '_self', 1);
INSERT INTO `resource` VALUES (15, 2, '13', '分步表单', 'fa fa-navicon', 'page/form-step', '_self', 2);
INSERT INTO `resource` VALUES (16, 2, '1', '登录模板', 'fa fa-flag-o', NULL, NULL, 6);
INSERT INTO `resource` VALUES (17, 2, '16', '登录-1', 'fa fa-stumbleupon-circle', 'page/login-1', '_self', 1);
INSERT INTO `resource` VALUES (18, 2, '16', '登录-2', 'fa fa-viacoin', 'page/login-2', '_self', 2);
INSERT INTO `resource` VALUES (19, 2, '1', '异常页面', 'fa fa-home', NULL, NULL, 7);
INSERT INTO `resource` VALUES (20, 2, '19', '404页面', 'fa fa-hourglass-end', 'page/404', '_self', 1);
INSERT INTO `resource` VALUES (21, 2, '1', '其它界面', 'fa fa-snowflake-o', NULL, NULL, 8);
INSERT INTO `resource` VALUES (22, 2, '21', '按钮示例', 'fa fa-snowflake-o', 'page/button', '_self', 1);
INSERT INTO `resource` VALUES (23, 2, '21', '弹出层', 'fa fa-shield', 'page/layer', '_self', 2);
INSERT INTO `resource` VALUES (24, 2, '7', '颜色选择', 'fa fa-dashboard', 'page/color-select', '_self', 3);
INSERT INTO `resource` VALUES (25, 2, '7', '下拉选择', 'fa fa-angle-double-down', 'page/table-select', '_self', 4);
INSERT INTO `resource` VALUES (26, 2, '7', '文件上传', 'fa fa-arrow-up', 'page/upload', '_self', 5);
INSERT INTO `resource` VALUES (27, 2, '7', '富文本编辑器', 'fa fa-edit', 'page/editor', '_self', 6);
INSERT INTO `resource` VALUES (28, 2, '10', '多级菜单', 'fa fa-meetup', NULL, NULL, 2);
INSERT INTO `resource` VALUES (29, 2, '28', '按钮1', 'fa fa-calendar', 'page/button', '_self', 1);
INSERT INTO `resource` VALUES (30, 2, '28', '按钮2', 'fa fa-snowflake-o', 'page/button', '_self', 2);
INSERT INTO `resource` VALUES (31, 2, '28', '按钮3', 'fa fa-snowflake-o', 'page/button', '_self', 3);
INSERT INTO `resource` VALUES (32, 2, '28', '表单4', 'fa fa-calendar', 'page/form', '_self', 4);
INSERT INTO `resource` VALUES (33, 1, '0', '后台管理', 'fa fa-gear', NULL, NULL, 40);
INSERT INTO `resource` VALUES (34, 2, '33', '用户中心', 'fa fa-users', NULL, NULL, 1);
INSERT INTO `resource` VALUES (35, 2, '34', '平台用户', 'fa fa-user', 'admin/users/user/list', '_self', 1);
INSERT INTO `resource` VALUES (36, 2, '34', '验证码查询', 'fa fa-newspaper-o', 'admin/users/captcha/list', '_self', 2);
INSERT INTO `resource` VALUES (37, 2, '33', '系统设置', 'fa fa-gears', NULL, NULL, 2);
INSERT INTO `resource` VALUES (38, 2, '37', '角色权限', 'fa fa-street-view', 'admin/setting/rolePermission/list', '_self', 1);
INSERT INTO `resource` VALUES (39, 2, '34', '登录日志', 'fa fa-area-chart', 'admin/setting/logLogin/list', '_self', 3);
INSERT INTO `resource` VALUES (40, 2, '37', '菜单管理', 'fa fa-window-maximize', 'admin/setting/menu/list', '_self', 2);
INSERT INTO `resource` VALUES (41, 2, '37', '接口管理', 'fa fa-hdd-o', '/doc.html', '_self', 3);
INSERT INTO `resource` VALUES (42, 2, '37', '定时任务', 'fa fa-tasks', 'admin/setting/task/list', '_self', 4);
INSERT INTO `resource` VALUES (43, 2, '33', '工作流程', 'fa fa-retweet ', NULL, NULL, 3);
INSERT INTO `resource` VALUES (44, 2, '43', '流程列表', 'fa fa-navicon', '/admin/workflow/list', '_self', 1);
INSERT INTO `resource` VALUES (45, 2, '43', '模型列表', 'fa fa-navicon', '/admin/workflow/model/list', '_self', 2);
INSERT INTO `resource` VALUES (46, 2, '43', '发起流程', 'fa fa-pencil', '/admin/workflow/add', '_self', 3);
INSERT INTO `resource` VALUES (47, 2, '43', '待办任务', 'fa fa-search', '/admin/workflow/task/list', '_self', 4);

-- ----------------------------
-- Table structure for resume
-- ----------------------------
DROP TABLE IF EXISTS `resume`;
CREATE TABLE `resume`  (
  `RID` int NOT NULL AUTO_INCREMENT COMMENT '简历编号，主键，自动增长',
  `Rname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简历人姓名',
  `Rgender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简历人性别',
  `Rimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简历图片',
  `Redu` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '面试人教育水平',
  `Rtel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简历人电话号码',
  `Remail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受面试通知的邮箱',
  `Rpositiondesired` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '期望职位',
  `Rlocation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '期望工作地点',
  `Rsalary` int NULL DEFAULT NULL COMMENT '薪资期望',
  `RworkExperience` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作经历',
  `RprojectExperience` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目经历',
  `Rcollege` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '面试人大学',
  `Rpro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '面试人专业',
  `Rpersonadvantage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自我描述',
  `Uid` int NULL DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`RID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resume
-- ----------------------------
INSERT INTO `resume` VALUES (1, '小王', '女', NULL, '大专', '1782324223423', '32431413@qq.com', '前端工程师', '北京', 5000, '吉禾日式餐饮有限公司     新媒体运营  2018-06至 2018-081. 吉禾日式餐厅是日式餐饮业界的一家知名公司，员工人数经超过2000人，门面店全国超过50家。2. 作为实习生加入运营推广部，参与公司线上的市场营销，团队超过50人，运营着20个公众号的矩阵，粉丝数量超过200万；3. 负责吉禾日式的微信公众号运营，撰写高质量原创文章，策划并输出季节性活动福利，关注各维度数据，保持用户粘性和增加到店人数；4. 实习期间，推文平均阅读量1k+，阅读量5k+的推文有2篇，2个月内为公司增粉5K+；5. 负责创建并运营3个微信渠道的福利社群，吸引超过1000名顾客加入\r\n', '工作时间：2010-04到2013-10\r\n\r\n公司名称：网络科技有限公司 | 所在部门：高级软件工程师，兼任项目组组长 \r\n\r\n工作描述：\r\n1、主要负责华夏银行大集中项目银联组的项目开发、维护、支持工作；\r\n2、协助项目经理管理组内的日常工作以及工作进度；\r\n3、主要负责的系统包括：B2C、柜面通、银联他代本、信用卡、银联清算等系统；\r\n4、对技术平台进行功能完善，形成相关技术文档；\r\n5、为在技术平台上进行的应用系统开发提供技术支持与开发指导；\r\n6、 参与改造以及银行卡刷卡手续费调整等项目的开发、测试、上线工作；', '理工大学', '软件工程', '本人已有五年的财务工作经验，熟悉做账，报税，开票，支票上账等一系列财务工作流程。有八年的办公室文员工作经验，熟悉人员合同备案，终结，解除合同关系，办理统筹保险等办事流程，并能兼顾企业工商营业执照年检工作，及组织机构代码年检等事宜。', 1);
INSERT INTO `resume` VALUES (2, '王富贵', '男', NULL, '本科', '177823847293', '23461397@qq.com', '产品经理', '上海', 10000, '系统工程有限公司担任职位:部门经理\r\n\r\n	　　工作内容:\r\n	　　1.负责大客户的售前支持;\r\n	    2.制定新产品的技术支持方案;\r\n	    3.负责部门日常管理，协调部门内部工作;\r\n	    4.负责销售人员和技术支持工程师的技术培训。\r\n\r\n	　　工作业绩:\r\n	　　主持开发了如下网络系统工程的技术支持方案:\r\n	\r\n	　　1、大型书店系统查询网络工作\r\n	　　2、国际展览中心网络系统工程\r\n	　　3、商业银行网络改造工程', '工作时间：2010-08到2012-03\r\n\r\n公司名称：简历本管理咨询有限公司 | 所在部门： | 所在岗位：项目管理经理\r\n\r\n工作职责：\r\n1、负责立项后制定《项目计划》并保证该计划的合理性和现实性；\r\n2、负责项目执行、项目监控和项目验收等阶段的执行\r\n3、负责带领团队进行需求分析，组织对设计进行评审；\r\n4、负责分配系统设计任务，包括软件设计、硬件设计、用户界面设计、数据库设计等。\r\n5、负责领导及指导项目团队，在工作效率与工作质量以及项目团队的有效合作等各方面\r\n6、负责对团队的工作标准、制度、项目管理规范等制度建设、实施及管理；\r\n6、负责项目的质量、成本、进度，完成公司要求的项目质量保证制度，确保项目质量、成本和进度目标的达成，分析偏差，采取纠正', '青岛大学', '软件外包', '有很好的统筹潜力和协调沟通潜力，配合领导完成全面工作并把领导的思想更好地传达给每一位销售经理。在工作中获得过很高的评价及良好的客户满意度。思想严密、逻辑清晰、性格开朗、待人真诚、具有强烈的职责心和事业心，具备很好的分析和解决问题的潜力。喜欢理解难度较大的工作挑战，精通各种办公软件，英文口语流利。', 2);
INSERT INTO `resume` VALUES (3, '小王男朋友', '女', NULL, '大专', '178342342', '32422342@qq.com', '代码工作', '北京', 20000, '有限公司担任职位:系统工程师\r\n\r\n	　　工作内容:\r\n\r\n	　　1.负责为用户进行SUN工作站及UNIX系统集成项目支持、服务及培训;2.参与开发SUNSPARC兼容工作站;3.用户售前咨询。\r\n\r\n	　　工作业绩:\r\n\r\n	　　1、参与建立了连锁超市收银系统\r\n\r\n	　　2、编制公司内部人事财务管理系统', '工作时间：2013-02到至今\r\n\r\n公司名称：咨询有限公司 | 所在部门： | 所在岗位：执行专员/项目管理\r\n\r\n工作描述\r\n负责 IT 厂商的会议邀请、leads 挖掘、数据清洗等不同项目的执行工作\r\n根据项目计划表按时完成项目目标结果，包括：预算、过程监管、资源协调以及客户的日常沟通等工作；\r\n准确、及时的回复客户的邮件、电话、\r\n负责撰写项目数据日报、周报、月报、执行总结、常规方案等；\r\n使用 OA 系统，准确的完成项目预算、支付、开票等工作；\r\n外包商的管理；', '理工大学', '计算机', '在大学期间，我始终以提高自身的综合素质为目标，以自我的全面发展为努力方向，树立正确的人生观、价值观和世界观。为适应社会发展的需求，我认真学习各种专业知识，发挥自己的特长；挖掘自身的潜力，结合每年的暑期社会实践机会，从而逐步提高了自己的学习潜力和分析处理问题的潜力以及务必的协调组织和管理潜力；“学而知不足”是我大学期间学习和工作的动力，除了必修课之外，我还坚持自学了Office、Flash、FrontPage、Photoshop等多种专业软件。', 3);

-- ----------------------------
-- Table structure for resumestate
-- ----------------------------
DROP TABLE IF EXISTS `resumestate`;
CREATE TABLE `resumestate`  (
  `rsid` int NOT NULL AUTO_INCREMENT,
  `salary` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Pname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Fname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `status` int NOT NULL DEFAULT 0,
  `createtime` datetime(6) NOT NULL,
  `positionId` int NOT NULL,
  `receiver` int NOT NULL,
  PRIMARY KEY (`rsid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resumestate
-- ----------------------------
INSERT INTO `resumestate` VALUES (1, '25000元', '后端开发工程师', '什么公司', 2, '2020-12-21 16:02:16.632000', 1, 7);
INSERT INTO `resumestate` VALUES (10, '23-233元/天', 'JAVA开发实习生2', '什么公司', 0, '2020-12-28 23:03:58.775000', 4, 7);
INSERT INTO `resumestate` VALUES (11, '20-35k', '跨境电商总经理', '什么公司', 0, '2020-12-29 09:05:24.433000', 7, 7);
INSERT INTO `resumestate` VALUES (12, '16-20K', '高级运维工程师', '什么公司', 0, '2020-12-29 09:05:43.482000', 9, 8);
INSERT INTO `resumestate` VALUES (13, '15-25K', 'HTML5讲师', '什么公司', 0, '2020-12-29 09:07:58.458000', 5, 7);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色标识',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '中文名称',
  `ordinal` int NULL DEFAULT NULL COMMENT '排序',
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `mender` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '开发人员', 1, '开发人员', b'0', 'hs', '2019-07-17 16:44:28', NULL, '2019-09-12 12:44:49');
INSERT INTO `role` VALUES (2, '后台管理', 2, '管理员', b'0', 'hs', '2019-08-26 09:59:15', NULL, '2019-09-08 14:16:46');
INSERT INTO `role` VALUES (3, '测试1', 3, '测试常规管理1', b'1', '18512345671', '2019-09-01 12:17:10', NULL, '2019-09-01 13:16:20');
INSERT INTO `role` VALUES (4, '测试2号', 4, '12', b'0', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `role` VALUES (5, '测试1号', 5, '212', b'0', '18512345671', '2019-09-01 12:26:05', NULL, '2019-09-08 14:16:20');
INSERT INTO `role` VALUES (6, '测试3号', 6, '测试12', b'0', '18512345671', '2019-09-01 12:45:38', NULL, '2019-09-01 13:16:02');
INSERT INTO `role` VALUES (7, '默认角色', 7, '默认角色', b'0', '18512345671', '2019-09-08 14:16:09', NULL, '2019-09-09 18:26:51');

-- ----------------------------
-- Table structure for roles_account
-- ----------------------------
DROP TABLE IF EXISTS `roles_account`;
CREATE TABLE `roles_account`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `account_id` bigint NOT NULL COMMENT '账户',
  `role_id` bigint NOT NULL COMMENT '角色',
  `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `mender` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles_account
-- ----------------------------
INSERT INTO `roles_account` VALUES (1, 1, 1, '开发人员', b'1', 'hs', '2019-07-17 16:45:22', NULL, NULL);
INSERT INTO `roles_account` VALUES (2, 2, 1, NULL, b'1', NULL, '2019-09-12 15:38:39', NULL, NULL);
INSERT INTO `roles_account` VALUES (3, 1, 1, NULL, b'1', NULL, '2020-12-12 19:00:22', NULL, NULL);

-- ----------------------------
-- Table structure for roles_hr
-- ----------------------------
DROP TABLE IF EXISTS `roles_hr`;
CREATE TABLE `roles_hr`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `account_id` bigint NOT NULL COMMENT '账户',
  `role_id` bigint NOT NULL COMMENT '角色',
  `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `mender` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles_hr
-- ----------------------------
INSERT INTO `roles_hr` VALUES (1, 1, 1, '开发人员', b'1', 'hs', '2019-07-17 16:45:22', NULL, NULL);
INSERT INTO `roles_hr` VALUES (2, 2, 1, NULL, b'1', NULL, '2019-09-12 15:38:39', NULL, NULL);
INSERT INTO `roles_hr` VALUES (3, 1, 1, NULL, b'1', NULL, '2020-12-12 19:00:22', NULL, NULL);
INSERT INTO `roles_hr` VALUES (4, 1, 1, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (5, 1, 2, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (6, 1, 4, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (7, 1, 5, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (8, 1, 6, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (9, 1, 7, NULL, b'1', NULL, '2020-12-18 00:13:18', NULL, NULL);
INSERT INTO `roles_hr` VALUES (10, 2, 4, NULL, b'0', NULL, '2020-12-18 00:14:43', NULL, NULL);
INSERT INTO `roles_hr` VALUES (11, 2, 6, NULL, b'0', NULL, '2020-12-18 00:14:43', NULL, NULL);
INSERT INTO `roles_hr` VALUES (12, 6, 7, NULL, b'0', NULL, '2020-12-18 00:21:08', NULL, NULL);
INSERT INTO `roles_hr` VALUES (13, 1, 1, NULL, b'1', NULL, '2020-12-18 00:23:21', NULL, NULL);
INSERT INTO `roles_hr` VALUES (14, 1, 2, NULL, b'1', NULL, '2020-12-18 00:23:21', NULL, NULL);
INSERT INTO `roles_hr` VALUES (15, 1, 4, NULL, b'1', NULL, '2020-12-18 00:23:21', NULL, NULL);
INSERT INTO `roles_hr` VALUES (16, 1, 1, NULL, b'0', NULL, '2020-12-18 00:25:33', NULL, NULL);
INSERT INTO `roles_hr` VALUES (17, 1, 2, NULL, b'0', NULL, '2020-12-18 00:25:33', NULL, NULL);
INSERT INTO `roles_hr` VALUES (18, 1, 4, NULL, b'0', NULL, '2020-12-18 00:25:33', NULL, NULL);
INSERT INTO `roles_hr` VALUES (19, 1, 5, NULL, b'0', NULL, '2020-12-18 00:25:33', NULL, NULL);

-- ----------------------------
-- Table structure for sys_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `sex` bit(1) NULL DEFAULT NULL COMMENT '(1:男,0:女)',
  `birth_data` datetime(0) NULL DEFAULT NULL COMMENT '出生日期',
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `password` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `is_locked` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否锁定（0:未锁定;1:已锁定）',
  `login_count` int NULL DEFAULT NULL COMMENT '登录次数',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '上次登录时间',
  `note` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `mender` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统账户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_account
-- ----------------------------
INSERT INTO `sys_account` VALUES (1, '18512345671', 'Sasnhi', b'1', '2019-08-27 00:00:00', 'http://rapid-boot.oss-cn-hangzhou.aliyuncs.com/201909/jpeg/20190908122031.jpeg?Expires=1883276432&OSSAccessKeyId=LTAInLUy8ia23gd5&Signature=pb0c6zgHTUe4QZmn2PuQBHoRkZk%3D', 'afdd0b4ad2ec172c586e2150770fbf9e', 'shljhs@foxmail.com', b'0', 394, '2020-12-18 00:26:27', '开发用户', b'0', '18512345671', '2019-08-18 12:43:06', NULL, '2020-12-18 00:26:27');
INSERT INTO `sys_account` VALUES (2, '18512345672', 'leilei', b'1', NULL, '/images/logo.png', 'e10adc3949ba59abbe56e057f20f883e', '191041656@qq.com', b'0', 6, '2019-09-12 16:18:29', '', b'0', '13546606929', '2019-09-10 11:58:17', NULL, '2020-12-18 20:34:40');
INSERT INTO `sys_account` VALUES (3, '18512345673', '18512345673', NULL, NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, b'0', NULL, NULL, NULL, b'1', NULL, NULL, NULL, NULL);
INSERT INTO `sys_account` VALUES (4, 'admin', 'admin', b'1', NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, b'0', 28, '2020-12-20 12:42:16', NULL, b'0', 'admin', NULL, NULL, '2020-12-20 12:42:16');
INSERT INTO `sys_account` VALUES (5, 'sa', 'sa', b'0', NULL, NULL, 'c12e01f2a13ff5587e1e9e4aedb8242d', '191041656@qq.com', b'0', 3, '2020-12-14 22:28:09', NULL, b'0', NULL, NULL, NULL, '2020-12-17 19:07:32');
INSERT INTO `sys_account` VALUES (6, '15621417563', 'sss', b'1', NULL, NULL, 'ss', '191041656@qq.com', b'0', NULL, NULL, NULL, b'1', NULL, NULL, NULL, NULL);
INSERT INTO `sys_account` VALUES (7, '15106830253', '15106830253', NULL, NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, b'0', 6, '2020-12-16 19:05:00', NULL, b'1', '15106830253', '2020-12-16 09:26:12', NULL, '2020-12-16 19:05:00');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色标识',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '中文名称',
  `ordinal` int NULL DEFAULT NULL COMMENT '排序',
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `mender` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '开发人员', 1, '开发人员', b'0', 'hs', '2019-07-17 16:44:28', NULL, '2019-09-12 12:44:49');
INSERT INTO `sys_role` VALUES (2, '后台管理', 2, '管理员', b'0', 'hs', '2019-08-26 09:59:15', NULL, '2019-09-08 14:16:46');
INSERT INTO `sys_role` VALUES (3, '测试1', 3, '测试常规管理1', b'1', '18512345671', '2019-09-01 12:17:10', NULL, '2019-09-01 13:16:20');
INSERT INTO `sys_role` VALUES (4, '测试2号', 4, '12', b'0', '18512345671', '2019-09-01 12:19:37', NULL, NULL);
INSERT INTO `sys_role` VALUES (5, '测试1号', 5, '212', b'0', '18512345671', '2019-09-01 12:26:05', NULL, '2019-09-08 14:16:20');
INSERT INTO `sys_role` VALUES (6, '测试3号', 6, '测试12', b'0', '18512345671', '2019-09-01 12:45:38', NULL, '2019-09-01 13:16:02');
INSERT INTO `sys_role` VALUES (7, '默认角色', 7, '默认角色', b'0', '18512345671', '2019-09-08 14:16:09', NULL, '2019-09-09 18:26:51');

-- ----------------------------
-- Table structure for sys_roles_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_account`;
CREATE TABLE `sys_roles_account`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `account_id` bigint NOT NULL COMMENT '账户',
  `role_id` bigint NOT NULL COMMENT '角色',
  `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_deleted` bit(1) NOT NULL COMMENT '逻辑删除',
  `creator` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `mender` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改者',
  `modify_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账户角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_roles_account
-- ----------------------------
INSERT INTO `sys_roles_account` VALUES (1, 1, 1, '开发人员', b'1', 'hs', '2019-07-17 16:45:22', NULL, NULL);
INSERT INTO `sys_roles_account` VALUES (2, 2, 1, NULL, b'1', NULL, '2019-09-12 15:38:39', NULL, NULL);
INSERT INTO `sys_roles_account` VALUES (3, 1, 1, NULL, b'1', NULL, '2020-12-12 19:00:22', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `Uid` int NOT NULL AUTO_INCREMENT COMMENT '用户编号，主键，自动增长',
  `UName` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `UPwd` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `UQuestion` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码查询问题',
  `Uicon` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `Ugender` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户性别',
  `Ubirth` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户生日',
  `Uedu` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户受教育程度',
  `Uexp` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户工作经验',
  `UAnswer` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码查询答案',
  `Hduty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户职务默认为求职者',
  `Utel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户电话',
  `Uemail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户电子邮箱',
  PRIMARY KEY (`Uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'mxz', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'lxh01.gif', '男', '2000/3/26', '本科', '1年', NULL, NULL, '17866629660', '522@qq.com');
INSERT INTO `users` VALUES (2, 'mmm', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'lxh02.gif', '女', '2000/3/26', '本科', '5年', NULL, NULL, '17866688888', '523@qq.com');
INSERT INTO `users` VALUES (3, 'xxx', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'lxh03.gif', '男', '2000/3/26', '本科', '2年', NULL, NULL, '17866699999', '527@qq.com');
INSERT INTO `users` VALUES (4, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd', NULL);
INSERT INTO `users` VALUES (5, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '156214175633', NULL);
INSERT INTO `users` VALUES (6, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15621417563', NULL);
INSERT INTO `users` VALUES (7, '刘萌', 'f379eaf3c831b04de153469d1bec345e', NULL, 'lxh02.gif', NULL, NULL, NULL, NULL, NULL, NULL, '17866635047', NULL);

SET FOREIGN_KEY_CHECKS = 1;
