# Online Job Hunting Platform

#### 介绍
Online Job Hunting Platform
大三上学期项目

#### 软件架构
利用springboot的快速开发，采用springboot2.2.2、spring、SpringMvc、mybatis-plus、swagger、thymeleaf、layui、Lombok 技术开发;使用mysql数据源


#### 安装教程

1. 运行sql脚本，使用maven构建项目
2. 安装lombok插件
3. util.SendCaptchaUtil填入申请的阿里云短信服务key
4. codes.CaptchaEnum填入申请的短信模板

#### 项目预览

1.  后台部分
        使用前端框架<a href="http://www.pearadmin.com/">layui</a>+ thymeleaf+
        后端技术SpringBoot+spring mvc+mybatis-puls+Swagger+spirngWebsocker+mysql+ Lombok
        主要功能
    - [√]浏览系统推荐本地景点
    -  [√]利用webSoket,log-slf4j实现打印控制台日志到云端；
    -  [√]使用AES和RS混合加密对API加密；
    -  [√]接入阿里云短信SDK实现手机登录； 
    -  [√]实现ErrorPageRegistrar接口配置错误页面；
    -  [√]使用Swagger描述api详细信息，构建api文档；
    -  [√]将两个及以上对象抽象为VO实体
    -  [√]利用AOP思想及自定义日志注解实现用户,hr，管理员登录时候将信息写入登录日志；
    -  [√]使用Enum描述状态码错误码；
    -  [√]使用cookie+ip+图片验证码的方式防止恶意发生短信；
    -  [√]使用JWT实现无状态实现身份认证(开发ing)，使用mybatis分页查询
    -  [√]使用restful风格API；
    -  [√]调用位置公共API获取用户登录时ip地址；
    -  [√]为用户权限分配,登录许可;
    -  [√]用户的各项信息的增删改查；
    -  
        登录账户：admin 密码：admin
    <P align="center">
    <img src="./git_img/11.png" style="zoom:80%"/>
    <img src="./git_img/log.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-13-37.png" style="zoom:50%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-14-44.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-17-19.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-17-55.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-18-46.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-19-48.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-20-10.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-22-58.png" style="zoom:80%"/>
    <img src="./git_img/Snipaste_2021-04-17_15-23-32.png" style="zoom:80%"/>
    </p>

    


2.  前台部分
    -  [√]企业招聘人员使用账号密码登录后可进入企业招聘人员个人中心。
    -  [√]点击“修改信息”可进入修改信息页面，在此页面内可以进行对个人信息
    -  [√]点击“发布职位”可以发布新的职位需求。
    -  [√]企业招聘人员随时更新或删除一个职位需求
    -  [√]点击某个需求的“查看更多”按钮可查看该职位需求的详细信息
    -  [√]在此页面内可对申请该职位的人进行操作，选择是否同意或拒绝他的申请
    -       登录账户：ice 密码：ice
    <P align="center">
    <img src="./git_img/1.png" style="zoom:50%"/>
    <img src="./git_img/2.png" style="zoom:50%"/>
    <img src="./git_img/3.png" style="zoom:50%"/>
    <img src="./git_img/4.png" style="zoom:50%"/>
    <img src="./git_img/5.png" style="zoom:50%"/>
    <img src="./git_img/6.png" style="zoom:50%"/>
    <img src="./git_img/7.png" style="zoom:50%"/>
    <img src="./git_img/8.png" style="zoom:50%"/>
    <img src="./git_img/9.png" style="zoom:50%"/>
    <img src="./git_img/10.png" style="zoom:50%"/>
    <img src="./git_img/14.png" style="zoom:50%"/>
    <img src="./git_img/12.png" style="zoom:50%"/>
    </p>
3.  xxxx

