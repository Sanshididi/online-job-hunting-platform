# Thymeleaf + Spring

## 介绍
本工程是一个简单的Thymeleaf的例子，用于向同学们展示如何快速上手Thymeleaf + Spring。学习本工程需要同学们对Spring 和 Thymeleaf有一定的了解，不过本工程会尽量清晰明了，降低同学们的学习门槛。

## 运行
同学们可以用工程管理工具Maven或者Gradle，或者IDE（集成开发环境）来运行本工程。这里只对命令行方式做简单介绍。Maven和Gradle工具需要同学们先行下载，具体方式不在赘述。
### Maven
编译+运行
```
mvn spring-boot:run
```


### 尝试
如果程序没有报错就可以登陆链接，打开浏览器输入网址。没有浏览器的同学也可以用curl命令来获取网页内容。此教程默认监听8080端口，在云环境中运行项目的同学根据自己的实际情况修改。

```
http://127.0.0.1:8080
curl http://127.0.0.1:8080
```

进入动态网址，用户可以在后面添加参数来返回不同的结果。用户的请求会被GreetingController.java接收，并根据name的值来返回语句。


当没有参数时，默认name值为World
```
http://127.0.0.1:8080/greeting
curl http://127.0.0.1:8080/greeting
```
返回结果为"Hello, World!"

参数为 name=1
```
http://127.0.0.1:8080/greeting?name=1
curl http://127.0.0.1:8080/greeting?name=1
```
返回结果为"Hello, 1!"

参数为 name=Jack
```
http://127.0.0.1:8080/greeting?name=Jack
curl http://127.0.0.1:8080/greeting?name=Jack
```
返回结果为"Hello, Jack!"


## 代码解读


工程结果如图所示：

```
.
├── README.md
├── pom.xml
└── src
    └── main
        ├── java
        │       └── com
        │             └── example
        │                 └── servingwebcontent
        │                     ├── GreetingController.java //Controller，用于接收不同的网页映射
        │                     └── ServingWebContentApplication.java //程序启动入口
        └── resources
            ├── static //静态页面
            │         └── index.html  //首页 http://127.0.0.1:8080
            └── templates//动态模板页面
                └── greeting.html // http://127.0.0.1:8080/greeting
11 directories, 15 files

```

### 延伸阅读
* Thymeleaf官网： https://www.thymeleaf.org/documentation.html
* Java工程脚手架： https://start.aliyun.com


